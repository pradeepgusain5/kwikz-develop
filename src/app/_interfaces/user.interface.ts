export interface ITopInvitees {
    email: string;
    fname: string;
    id: number;
    invited_confirmed: number;
    is_verified: number;
    lname: string;
    mobile: string;
    profile_pic: null|string;
    referral_id: null|string;
}
