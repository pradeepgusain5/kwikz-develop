import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject, AfterViewInit } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastNotifications } from 'ngx-toast-notifications';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { HttpServiceService } from '../../_services';
import { Meta, Title } from '@angular/platform-browser';
import { Http } from '@angular/http';
declare var $: any;
declare var gapi: any;

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.scss']
})
export class MyaccountComponent implements OnInit, AfterViewInit {

  public draftListing: Array<any> = [];
  bucket: any = new S3({
    accessKeyId: environment.accessKeyId,
    secretAccessKey: environment.secretAccessKey,
    region: environment.region
  });
  userId: string;
  authConfig: any = {};
  public loggedIn: boolean; // user loggedIn status
  // tslint:disable-next-line:variable-name
  public mobile_verified = true;
  public postThumbnailBaseUrl: string = environment.s3ImageUrl;
  public carPostUrl: string = environment.carPostUrl;
  public feedback = { ratingAll: [], ratingAsBuyer: [], ratingAsSeller: [] };
  public mobileVerify = { showVerifyMsg: false, showOtp: true, fullDiv: false };
  public credit: number;
  public formData: string;
  public customerName: string;
  public customerLastName: string;
  public customerRating: string;
  public customerEmail: string;
  public customerPhone: string;
  // tslint:disable-next-line:variable-name
  public profile_pic: string;
  public image: any;
  public selectedImage: any;
  public customerCity: string; // address variables start
  public customerRegion: string;
  public customerStreet: string;
  public customerLocality: string;
  public customerZipcode: string; // address variables ends
  // tslint:disable-next-line:variable-name
  public payment_instructions: string;
  // tslint:disable-next-line:variable-name
  public payment_options = [];
  public customerCredit: string;
  public customerCurrentPassword = '';
  public customerPassword = '';
  public transactions: any[];
  public customerMobilePrivacySettings: boolean;
  public customerEmailPrivacySettings: boolean;
  public customerPaymentInstructions: string;
  public userSoldItems: any = [];
  public userUnsoldItems: any = [];
  public userItemSelling: any = [];
  public watchListItems: any = [];
  public addressCallMethod: number;
  public memberSince: string;
  public user: any;
  // tslint:disable-next-line:variable-name
  public is_dealer: any;
  // tslint:disable-next-line:variable-name
  public is_verified_dealer: any;
  // tslint:disable-next-line:variable-name
  public user_id: any;
  // public counter: number;
  public contentTransaction: any[] = new Array();
  collection: any[];
  autoSuggestAddress: any;
  base64ProfileImageString = '';
  otp: any;
  checkUser = false;
  auth2: any;
  divtableresult: any;
  googleContacts: any = [];
  showGoogleContactModel = false;
  userFilter: any = { id: '' };
  public showMobileVerification = true;
  public loader = false;

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    // tslint:disable-next-line: deprecation
    private httpClient: Http,
    public meta: Meta,
    public title: Title,
    public _DomSanitizationService: DomSanitizer,
    private router: Router,
    private http: HttpServiceService,
    private toasts: ToastNotifications
  ) {

  }

  public autoCompleteSettings: any = {
    showRecentSearch: false,
    geoCountryRestriction: ['nz'],
    searchIconUrl: 'http://downloadicons.net/sites/default/files/identification-search-magnifying-glass-icon-73159.png'
  };

  ngOnInit() {
    this.title.setTitle('Profile | Kwikz');
    this.meta.updateTag({
      // tslint:disable-next-line:max-line-length
      name: 'description', content: 'Kwikz is a product designed by the vision to provide a smooth car buying and selling to New Zealanders. Zero success fee & re-list until sold. Signup!'
    })
    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,profile,myaccount'
    //   },

    // ]);
    this.loader = true;
    this.http.isLoggedFn().subscribe(x => this.loggedIn = x
    );
    if (!this.loggedIn) { // incase user not loggedIn, user redirecting to home page. A toaster msg should be there
      this.router.navigate(['/login']);
    }
    this.getUserData();
    this.authConfig = {
      client_id: environment.google_client_id,
      scope: 'https://www.googleapis.com/auth/contacts.readonly'
    };
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  ngAfterViewInit(): void {
  }

  getUserData() {
    this.http.get('user/detail').subscribe(data => {

      this.userId = data.user_detail.id;
      this.getPost();
      this.user = data;
      this.loader = false;
      this.customerName = data.user_detail.fname;
      this.customerLastName = data.user_detail.lname;
      this.customerRating = data.user_detail.rating === 0 ? null : String(Math.round(data.user_detail.rating));
      this.memberSince = data.user_detail.created_at;
      this.credit = data.user_detail.credit;
      this.mobile_verified = data.user_detail.mobile_verified ? true : false;
      if (data.user_detail.otp) {
        this.mobileVerify.showVerifyMsg = true;
        this.mobileVerify.showOtp = false;
      }
      this.customerEmail = data.user_detail.email;
      this.customerPhone = data.user_detail.mobile.split('+64');
      this.customerPhone = this.customerPhone[1];
      // tslint:disable-next-line:max-line-length
      this.profile_pic = data.user_detail.profile_pic ? data.user_detail.profile_pic : './assets/images/myaccount/profileImageNotAvailable.jpeg';
      this.customerCity = (data.user_address.length > 0) ? data.user_address[0].city_town : '';
      this.customerRegion = (data.user_address.length > 0) ? data.user_address[0].region_state : '';
      this.payment_instructions = data.user_detail.payment_instructions;
      this.customerPaymentInstructions = data.user_detail.payment_instructions;
      this.is_dealer = data.user_detail.is_dealer;
      if (data.dealer_business) {
        this.is_verified_dealer = data.dealer_business.is_verified;
      }
      if (this.is_dealer && this.is_verified_dealer) {
        this.localStorage.setItem('is_dealer', 'true');
      } else {
        this.localStorage.setItem('is_dealer', 'false');
      }
      this.localStorage.setItem('referral_id', data.user_detail.referral_id);
      this.user_id = data.user_detail.id;
      this.addressCallMethod = (data.user_address.length > 0) ? 0 : 1;
      this.transactions = data.user_transaction;
      this.payment_options = data.stripeData_cardLast4;
      this.customerMobilePrivacySettings = data.user_privacy[0].mobile_public_disable === '0' ? true : false;
      // tslint:disable-next-line:max-line-length
      this.customerEmailPrivacySettings = data.user_privacy[0].email_public_disable === '0' ? true : false; let locality = (data.user_address.length > 0) ? data.user_address[0].street.split(',') : '';
      this.customerStreet = (data.user_address.length > 0) ? locality[0].trim() : '';
      this.customerLocality = (data.user_address.length > 0) ? locality[1].trim() : '';
      this.customerZipcode = (data.user_address.length > 0) ? data.user_address[0].zipcode : '';
      this.secondaryFunctions();
    }, error => {
      this.toasts.next({ text: 'Something went wrong', caption: 'Fail', type: 'danger' });
      this.loader = false;
      this.localStorage.clear();
      this.http.loggedIn.next(this.localStorage.getItem('token') ? true : false);
      this.http.lastUrl.next('/myaccount');
      this.router.navigate(['/login']);
    }
    );
  }

  secondaryFunctions() {
    // calling secondary functions
    this.soldItems();
    this.unsoldItems();
    this.itemSelling();
    this.watchlist();
    this.feedbackFn();
  }

  feedbackFn() {
    this.http.get('user/rating').subscribe(data => { this.feedback = data; }, error => {
      this.toasts.next({ text: 'Unable to get feedback', caption: 'Fail', type: 'danger' });
    }
    );
  }
  verifyMobile() {
    this.http.get('mobile/verify').subscribe(data => {
      this.toasts.next({ text: 'SMS sent', caption: 'Success', type: 'success' });
    }, error => {
      this.toasts.next({ text: 'Unable to send SMS', caption: 'Fail', type: 'danger' });
    }
    );
    this.mobileVerify.showVerifyMsg = true;
    this.mobileVerify.showOtp = false;
  }

  verifyOtp(data: number) {
    this.http.post('mobile/otp', { otp: data }).subscribe(response => {
      if (response.otp_verified === true) {
        this.mobileVerify.fullDiv = true;
        this.toasts.next({ text: 'Mobile number verified successfully', caption: 'Success', type: 'success' });
      } else {
        this.toasts.next({ text: 'OTP entered is incorrect', caption: 'Fail', type: 'danger' });
      }
    }, error => { this.toasts.next({ text: 'Unable to verify OTP', caption: 'Fail', type: 'danger' }); }
    );
  }
  inviteUser() {
    let inviteUsersPostData = {
      emails: []
    };
    inviteUsersPostData.emails.push(this.customerCredit);
    if (this.customerCredit === '') {
      return; // validation
    }
    this.http.post('user/invite', inviteUsersPostData)
      .subscribe(
        response => {
          this.toasts.next({ text: 'You have invited users successfully', caption: 'Success', type: 'success' });
        },
        error => {
          this.toasts.next({ text: 'Unable to invite user', caption: 'Fail', type: 'danger' });
        }
      );
  }

  displayForm(params) {
    let displayState = document.getElementById('js-' + params);
    let displaySetting = displayState.style.display;

    if (displaySetting === 'block') {
      displayState.style.display = 'none';
    } else {
      displayState.style.display = 'block';
    }
  }

  goToDealerPage() {
    if (this.is_dealer) {
      this.router.navigate([`/dealer/${this.user_id}`]);
    } else {
      this.router.navigate([`/dealer/register`]);
    }
  }

  focusOnField(event, value: string, type: string, container: string) {
    if (value !== '' || value !== undefined) {
      $('#' + container + '-error').remove();
    }
  }

  itemSelling() {
    // this.http.get('user/itemselling')
    //   .subscribe(data => {
    //     this.userItemSelling = data.data;
    //   },
    //     error => { console.log('Unable to get ItemSelling Data'); }
    //   );

  }
  soldItems() {
    // this.http.get('user/itemsold')
    //   .subscribe(data => {
    //     this.userSoldItems = data.data;
    //   },
    //     error => { console.log('Unable to get SoldItem Data'); }
    //   );

  }
  unsoldItems() {
    // this.http.get('user/itemunsold')
    //   .subscribe(data => {
    //     this.userUnsoldItems = data.data;
    //   },
    //     error => { console.log('Unable to get UnsoldItems Data'); }
    //   );
  }
  watchlist() {
    this.http.get('user/watchlist')
      .subscribe(data => {
        this.watchListItems = data.data;
      },
        error => { console.log('Unable to get SoldItem Data'); }
      );
  }


  // address add & update
  autoCompleteCallback1(eventGoogle) {
    let formattedAddr = this.extractAddress(eventGoogle.data.address_components, eventGoogle.data.adr_address);
  }
  extractAddress(value, addressString) {
    // check for slashes in the premise address
    if (addressString !== '') {
      let checkForSlashes = addressString.split('<span class="street-address">');
      // console.log(checkForSlashes);
    }
    let addrLength = value.length;
    if (addrLength > 0) {
      this.customerStreet = '';
      for (let i = 0; i < addrLength; i++) {
        // console.log('arr ', value[i].types);
        // console.log('index ', value[i].types.indexOf('administrative_area_level_1'));

        // street
        // this.customerStreet = '';
        if (value[i].types.indexOf('subpremise') !== -1) {
          this.customerStreet += value[i].short_name;
        }
        if (value[i].types.indexOf('street_number') !== -1) {
          this.customerStreet += value[i].short_name + ' ';
        }
        if (value[i].types.indexOf('route') !== -1) {
          this.customerStreet += value[i].short_name;
        }
        if (value[i].types.indexOf('sublocality') !== -1) {
          this.customerLocality = value[i].short_name;
        }
        // city
        if (value[i].types.indexOf('locality') !== -1) {
          this.customerCity = value[i].short_name;
        }
        // region
        if (value[i].types.indexOf('administrative_area_level_1') !== -1) {
          this.customerRegion = value[i].short_name;
        }
        // postal code
        if (value[i].types.indexOf('postal_code') !== -1) {
          this.customerZipcode = value[i].short_name;
        }
      }
    }
  }
  addAddress(customerCity, customerRegion, customerLocality, customerStreet, customerZipcode) {
    let addressPostData = {
      city_town: customerCity,
      region_state: customerRegion,
      street: customerStreet + ', ' + customerLocality,
      zipcode: customerZipcode
    };

    this.http.post('user/address', addressPostData)
      .subscribe(
        response => {
          this.toasts.next({ text: 'Your address updated successfully', caption: 'Success', type: 'success' });
        },
        error => {
          this.toasts.next({ text: 'An error occurred while updating your address', caption: 'Fail', type: 'danger' });
        }
      );
  }
  updateAddress(customerCity, customerRegion, customerLocality, customerStreet, customerZipcode) {
    // console.log(customerCity + customerRegion + customerStreet + customerZipcode);
    let addressPostData = {
      city_town: customerCity,
      region_state: customerRegion,
      street: customerStreet + ', ' + customerLocality,
      zipcode: customerZipcode
    };
    this.http.post('user/update', addressPostData)
      .subscribe(
        response => {
          this.toasts.next({ text: 'Your address updated successfully', caption: 'Success', type: 'success' });
        },
        error => {
          this.toasts.next({ text: 'An error occurred while updating your address', caption: 'Fail', type: 'danger' });
        }
      );
  }


  // Profile Pic Upload
  handleFileSelect(evt) {
    this.image = evt.target.files[0];
    let reader = new FileReader();
    reader.readAsDataURL(this.image);
    this.selectedImage = window.URL.createObjectURL(this.image);
  }

  updateUserProfileImage() {
    let params = {
      Bucket: 'bhaskar-kwikz',
      // tslint:disable-next-line:max-line-length
      Key: 'user/' + this.userId + '/profile/' + Date.now() + '-' + Math.floor(Math.random() * (+9999999 - +100)) + +100 + '-' + this.image.name,
      Body: this.image,
      ACL: 'public-read',
      StorageClass: 'REDUCED_REDUNDANCY'
    };

    this.bucket.upload(params, (err, data) => {
      if (err) {
        console.log('There was an error uploading your file: ', err);
        return false;
      }
      this.profile_pic = data['Location'];
      this.http.post('image/upload_profile', { image: this.profile_pic })
        .subscribe(
          response => {
            this.toasts.next({ text: 'Your profile picture updated successfully', caption: 'Success', type: 'success' });

          },
          error => {
            this.toasts.next({ text: 'An error occurred while updating your profile picture', caption: 'Fail', type: 'danger' });
          });
      console.log('Image Uploaded successfully: ', data);
    });
  }


  // updating profile
  updateProfileName(firstName: string, lastName: string) {
    let postNames = {
      fname: firstName,
      lname: lastName
    };
    this.http.post('user/update', postNames)
      .subscribe(
        response => {
          this.toasts.next({ text: 'Your name updated successfully', caption: 'Success', type: 'success' });
        },
        error => {
          this.toasts.next({ text: 'An error occurred while updating your name', caption: 'Fail', type: 'danger' });
        });
  }

  updateProfile(value: string, type: string, container: string) {
    if (type === 'mobile') {
      value = '+64' + value;
    }
    let postData = { [type]: value };
    this.http.post('user/update', postData)
      .subscribe(
        response => {
          this.toasts.next({ text: `Your ${type} updated successfully`, caption: 'Success', type: 'success' });
        },
        error => {
          this.toasts.next({ text: `An error occurred while updating your ${type}`, caption: 'Fail', type: 'danger' });
        });
  }

  updateUserPrivacy() {
    let postPrivacyData = {
      mobile_public_disable: this.customerMobilePrivacySettings === true ? '0' : '1',
      email_public_disable: this.customerEmailPrivacySettings === true ? '0' : '1'
    };
    this.http.post('user/update', postPrivacyData)
      .subscribe(
        response => {
          this.toasts.next({ text: 'Your privacy setting updated successfully', caption: 'Success', type: 'success' });
          this.ngOnInit();
        },
        error => {
          this.toasts.next({ text: 'An error occurred while updating your privacy settings', caption: 'Fail', type: 'danger' });
        }
      );

  }
  updateUserPassword() {
    if ((this.customerCurrentPassword.toString().length > 4) && (this.customerPassword.toString().length > 4)) {
      let body = {
        new_password: this.customerPassword,
        current_password: this.customerCurrentPassword
      };
      this.http.post('user/passwordchange', body)
        .subscribe(
          response => {
            this.toasts.next({ text: 'Your password updated successfully', caption: 'Success', type: 'success' });
          },
          error => {
            this.toasts.next({ text: 'An error occurred while updating your password', caption: 'Fail', type: 'danger' });
          });
    } else {
      this.toasts.next({ text: 'Please check password should be not blank and min. of five character.', caption: 'Fail', type: 'danger' });
    }
  }

  googleSignIn() {
    this.showGoogleContactModel = true;
    gapi.client.setApiKey(environment.google_api_key);
    gapi.auth2.authorize(this.authConfig, this.handleAuthorization);
  }

  handleAuthorization = authorizationResult => {
    if (authorizationResult && !authorizationResult.error) {
      let url: string = 'https://www.google.com/m8/feeds/contacts/default/thin?' +
        'alt=json&max-results=500&v=3.0&access_token=' +
        authorizationResult.access_token;
      this.httpClient.get(url).subscribe(response => {
        this.fetchContacts(response);
      });
    }
  }

  fetchContacts(data) {
    this.googleContacts = [];
    this.showGoogleContactModel = true;
    let googleData = JSON.parse(data._body).feed.entry;
    for (let email of googleData) {
      let mail = {
        id: email.gd$email[0].address
      };
      this.googleContacts.push(mail);
    }
  }

  sendInvitation() {
    let inviteUsersPostData = {
      emails: []
    };
    for (let contact of this.googleContacts) {
      if (contact.share) {
        inviteUsersPostData.emails.push(contact.id);
      }
    }
    this.sendInvitationMail(inviteUsersPostData);
    this.showGoogleContactModel = false;
  }

  sendInvitationMail(mail) {
    this.http.post('user/invite', mail).subscribe(response => {
    },
    );
  }

  selectAllContact() {
    for (let contact of this.googleContacts) {
      contact.share = true;
    }
  }

  private getPost() {
    let dealer = {
      user_id: +this.userId
    };
    this.http.get('user/itemstandard').subscribe(
      data => {
        this.loader = false;
        this.draftListing = data.data.filter(e => e.approved === 0 && e.active === 0);
        this.userSoldItems = data.data.filter(e => e.trade_done === 1 && e.withdraw === 1);
        this.userUnsoldItems = data.data.filter(e => e.trade_done === 0 && e.withdraw === 1);
        this.userItemSelling = data.data.filter(e => e.trade_done === 0 && e.withdraw === 0);
      }, error => {
        this.loader = false;
        this.toasts.next({ text: 'Something went wrong', caption: 'Fail', type: 'danger' });
      }
    );
  }

}
