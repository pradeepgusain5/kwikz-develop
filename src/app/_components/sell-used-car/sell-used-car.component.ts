import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpServiceService } from '../../_services';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-sell-used-car',
  templateUrl: './sell-used-car.component.html',
  styleUrls: ['./sell-used-car.component.scss']
})
export class SellUsedCarComponent implements OnInit {

  public loggedIn: boolean;

  constructor(private http: HttpServiceService, private router: Router, public meta: Meta, public title: Title) { }

  ngOnInit() {
    this.title.setTitle('Sell Used Car Online: Selling Your Car is Going to be Smooth Ride | Kwikz');
    this.meta.updateTag({
      // tslint:disable-next-line: max-line-length
      name: 'description', content: 'Are You Looking to Selling Your Car Online? At Kwikz - Upload up to 20 Photos, No Hidden Cost, Direct Communication, Relist Until Sold.  List Your Car Free!'
    })
    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,sell used car'
    //   },

    // ]);
    this.http.isLoggedFn().subscribe(
      x => {
        this.loggedIn = x;
      }
    );
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  checkLogin() {
    if (this.loggedIn) {
      if (localStorage.getItem('is_dealer') === 'true') {
        this.router.navigate(['/sell-multi-car']);
      } else {
        this.router.navigate(['/sell-car/new']);
      }
    } else {
      if (localStorage.getItem('is_dealer') === 'true') {
        this.http.lastUrl.next('/sell-multi-car');
      } else {
        this.http.lastUrl.next('/sell-car/new');
      }
      this.router.navigate(['/login']);
    }
  }

}
