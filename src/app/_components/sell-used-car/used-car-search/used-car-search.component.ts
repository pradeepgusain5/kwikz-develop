import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { ToastNotifications } from 'ngx-toast-notifications';
import { HttpServiceService } from '../../../_services';
import { environment } from '../../../../environments/environment';
import { CanonicalService } from '../../../_services/canonicalUrl.service';
@Component({
  selector: 'app-used-car-search',
  templateUrl: './used-car-search.component.html',
  styleUrls: ['./used-car-search.component.scss']
})

export class UsedCarSearchComponent implements OnInit {
  loggedIn: boolean;
  resultPosts: any = [];
  imageBaseUrl: string = environment.s3ImageUrl;
  carPostUrl: string = environment.carPostUrl;
  nextUrl = '';
  lastPage = '';
  paginatedData = false;
  noOfPages: any = [];
  currentPage: any;
  postSkeleton = true;
  skeletonArray: any = [1, 2, 3, 4, 5];
  noResultFound = false;
  fitlerFormMake: any;
  fitlerFormModel: any;
  filterFormLocation: any;
  fitlerFormYearFrom: any;
  fitlerFormYearTo: any;
  fitlerFormKmsFrom: any;
  fitlerFormKmsTo: any;
  fitlerFormPriceFrom: any;
  fitlerFormPriceTo: any;
  filterFormFuelType: any;
  filterFormBodyType: any;
  filterFormTransmissionType: any;
  filterFormColor: any;
  filterFormlistingId: any;
  currentModelValue: any;
  currentMakeValue: any;
  filterData: any;
  currentTitleValue: any;
  showFilters = false;
  showPagination = false;
  public loader: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    public router: Router,
    private http: HttpServiceService,
    private toasts: ToastNotifications,
    public meta: Meta,
    public title: Title,
    private canonicalService: CanonicalService
  ) { }

  ngOnInit() {
    this.title.setTitle('Used Cars for Sale: Find Used Car, Buy Secondhand Cars in NZ | Kwikz');
    this.meta.updateTag({
      // tslint:disable-next-line:max-line-length
      name: 'description', content: 'Thinking of Buying a Car? Kwikz Helps to Find Your Dream Used Car. Find Used Cars, Search By Easy Filter Options, Buy Used Cars in New Zealand at Kwikz!'
    })
    // this.canonicalService.createCanonicalURL('https://www.kwikz.nz/used-car-search')
    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,used car search'
    //   },

    // ]);

    this.http.isLoggedFn().subscribe(
      x => this.loggedIn = x
    );

    // this.loader = true;
    this.activatedRoute.queryParams.subscribe(
      (params: Params) => {
        // tslint:disable-next-line:triple-equals
        if (JSON.stringify(params) != '{}') {
          this.showPagination = false;
          this.paginatedData = false;
          this.filterData = params;
          this.getFilterDataFromRoute();
          this.http.post('post/filter', params).subscribe(
            data => {
              this.postSkeleton = false;
              this.resultPosts = data;
              if (!(this.resultPosts.length > 0)) {
                this.noResultFound = true;
              }
            }, error => {
              this.postSkeleton = false;
              this.noResultFound = true;
              console.log(error);
            }
          );
        } else {
          this.showPagination = true;
          this.paginatedData = true;
          this.http.get('post/filterpaginated').subscribe(
            data => {
              this.loader = false;
              this.postSkeleton = false;
              this.resultPosts = data.data;
              if (!(this.resultPosts.length > 0)) {
                this.noResultFound = true;
              }
              this.currentPage = data.current_page;
              this.nextUrl = data.current_page + 1;
              this.lastPage = data.last_page;
              for (let i = 1; i <= +this.lastPage; i++) {
                this.noOfPages.push(i);
              }
            }, error => {
              this.loader = false;
              this.postSkeleton = false;
              this.noResultFound = true;
            }
          );
        }
      });
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  getFilterDataFromRoute() {
    this.currentTitleValue = this.filterData.keyword;
    this.currentMakeValue = this.filterData.make;
    this.currentModelValue = this.filterData.model;
    this.fitlerFormYearFrom = this.filterData.year_manufacture_from;
    this.fitlerFormYearTo = this.filterData.year_manufacture_to;
    this.fitlerFormKmsFrom = this.filterData.kilometers_used_from;
    this.fitlerFormKmsTo = this.filterData.kilometers_used_to;
    this.filterFormFuelType = this.filterData.fuel_type;
    this.filterFormBodyType = this.filterData.body_style;
    this.filterFormTransmissionType = this.filterData.transmission;
    this.filterFormColor = this.filterData.color;
    this.filterFormlistingId = this.filterData.listing_id;
    this.fitlerFormPriceFrom = this.filterData.price_from;
    this.fitlerFormPriceTo = this.filterData.price_to;
    this.filterFormLocation = this.filterData.location;
  }

  loadNextPagePosts(page) {
    this.resultPosts = [];
    this.loader = true;
    this.http.get('post/filterpaginated?page=' + page).subscribe(
      data => {
        this.resultPosts = data.data;
        this.currentPage = data.current_page;
        this.loader = false;
      }, error => {
        // console.log('error');
        this.loader = false;
      }
    );
  }

  loadMorePosts() {
    this.http.get('post/filterpaginated?page=' + this.nextUrl).subscribe(
      data => {
        this.resultPosts = this.resultPosts.concat(data.data);
        this.nextUrl = data.current_page + 1;
        this.lastPage = data.last_page;
      }, error => {
        // console.log('error');
      }
    );
  }

  addToWishlist(id) {
    if (!this.loggedIn) {
      if (confirm('Login is required to add post to watchList. Login now?')) {
        this.http.lastUrl.next(this.router.url);
        this.router.navigate(['/login']);
      }
    } else {
      this.http.post('user/watchlistadd', { post_id: id }).subscribe(
        data => {
          this.toasts.next({ text: 'Post added to watchList', caption: 'Success', type: 'success' });
        },
        error => {
          let errorMsg = JSON.parse(error._body);
          console.log(errorMsg.msg);
          this.toasts.next({ text: errorMsg.msg, caption: 'Fail', type: 'danger' });
        }
      );
    }
  }

  getFilterPost() {
    this.noResultFound = false;
    let filterData = {
      make: this.currentMakeValue ? this.currentMakeValue : '',
      model: this.currentModelValue ? this.currentModelValue : '',
      year_manufacture_from: this.fitlerFormYearFrom ? this.fitlerFormYearFrom : '',
      year_manufacture_to: this.fitlerFormYearTo ? this.fitlerFormYearTo : '',
      kilometers_used_from: this.fitlerFormKmsFrom ? this.fitlerFormKmsFrom : '',
      kilometers_used_to: this.fitlerFormKmsTo ? this.fitlerFormKmsTo : '',
      fuel_type: this.filterFormFuelType ? this.filterFormFuelType : '',
      body_style: this.filterFormBodyType ? this.filterFormBodyType : '',
      transmission: this.filterFormTransmissionType ? this.filterFormTransmissionType : '',
      color: this.filterFormColor ? this.filterFormColor : '',
      listing_id: this.filterFormlistingId ? this.filterFormlistingId : '',
      price_from: this.fitlerFormPriceFrom ? this.fitlerFormPriceFrom : '',
      price_to: this.fitlerFormPriceTo ? this.fitlerFormPriceTo : '',
      location: this.filterFormLocation ? this.filterFormLocation : ''
    };

    if (filterData.make || filterData.model) {
      this.router.navigate([environment.filterUrl], { queryParams: filterData });
    } else {
      this.router.navigate(['/']).then(() => this.router.navigate([environment.filterUrl], { queryParams: filterData }));
    }
  }
}
