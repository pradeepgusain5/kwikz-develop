import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ToastNotifications } from 'ngx-toast-notifications';
import * as moment from 'moment';
declare var $: any;
import { bool } from 'aws-sdk/clients/signer';
import { HttpServiceService } from '../../../_services';
import { environment } from '../../../../environments/environment';
import { Meta, Title } from '@angular/platform-browser';
import { TitleCasePipe } from '@angular/common';


@Component({
  selector: 'app-used-car',
  templateUrl: './used-car.component.html',
  styleUrls: ['./used-car.component.scss']
})
export class UsedCarComponent implements OnInit {
  loggedIn: boolean;
  postId: number;
  filterUrl: string = environment.filterUrl; // filter baseURL
  sellerProfileUrl: string = environment.userProfileImage; // image baseURl
  postData = {
    post: {
      title: '', updated_at: '', location: '', views_count: '', watchers: '', amount: '', desc: '', withdraw: '', trade_done: 0
    },
    extra_fields: {
      // tslint:disable-next-line:max-line-length
      make: '', model: '', model_detail: '', kilometers_used: '', exterior_color: '', transmission: '', cylinders: '', fuel_type: '', no_of_owners: '',
      // tslint:disable-next-line:max-line-length
      best_contact_time: '', abs_brakes: '', air_conditioning: '', alarm: '', alloy_wheels: '', central_locking: '', driver_airbag: '', passenger_airbag: '', power_steering: '',
      // tslint:disable-next-line:max-line-length
      sunroof: '', towbar: '', four_wd: '', stereo: '', year_manufacture: '', Plate_no_VIN: '', body_style: '', engine_size: '', Wof_expires: '', registration_exp: '', doors: '', import_history: ''
    },
    images: [],
    sellerInfo: { id: '', fname: '', lname: '', email: '', created_at: '', mobile: '' },
    qnS: [],
    sellerPrivacy: { mobile: '', email: '' }
  };
  // tslint:disable-next-line:variable-name
  is_dealer = false;
  // tslint:disable-next-line:variable-name
  dealer_route: any = '';
  noImageData = ['./assets/images/nocarimage.jpg']; // when post don't have image
  sellerRating: string;
  sellerProfileImage: string;
  breadCrums = { region: '', city: '', make: '', model: '', year: '' };
  role = 'buyer';
  answerUnanswered: any = [];
  contactSeller = { mobile: '', price: '', message: '', sellerEmail: '', buyerEmail: '', sellerName: '', buyerName: '', postUrl: '' };
  validationFailedContactSeller = false;
  validationFailedPostQuestion = false;
  validationFailedPostAnswer = false;
  postQuestion = '';
  postAnswer = '';
  postUpdateRoute: string = environment.SellCreate;
  WatchListAlreadyAdded = false;
  carJamAffUrl = '';
  postExpired = false;
  postStartDate;
  userRegisteredSince;
  urlContainCity = true;
  public loader = true;

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private titlecasePipe: TitleCasePipe,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private http: HttpServiceService,
    private toasts: ToastNotifications,
    public meta: Meta,
    public title: Title
  ) {
    this.route.params.subscribe(params => {
      this.postId = params.id;
      if (params.regioncity.includes('-')) {
        let arrayData = params.regioncity.split('-');
        this.breadCrums.region = arrayData[0];
        this.breadCrums.city = arrayData[1];
      } else {
        this.breadCrums.region = params.regioncity;
        this.urlContainCity = false;
      }
      this.breadCrums.make = params.make;
      this.breadCrums.model = params.model;
      this.breadCrums.year = params.year;
    }
    );
    this.scrollUpToTop();
  }

  ngOnInit() {
    this.breadCrums.region = this.titlecasePipe.transform(this.breadCrums.region);
    this.breadCrums.city = this.titlecasePipe.transform(this.breadCrums.city);
    this.breadCrums.make = this.titlecasePipe.transform(this.breadCrums.make);
    this.breadCrums.model = this.titlecasePipe.transform(this.breadCrums.model);
    this.breadCrums.year = this.titlecasePipe.transform(this.breadCrums.year);

    // tslint:disable-next-line:max-line-length
    this.title.setTitle(`Used ${this.breadCrums.make} ${this.breadCrums.model} ${this.breadCrums.year} for Sale in ${this.breadCrums.city} ${this.breadCrums.region} | Kwikz`);

    this.http.isLoggedFn().subscribe(
      x => this.loggedIn = x
    );

    this.loader = true;
    this.http.post('post/show', { post_id: this.postId }).subscribe(data => {
      // if (!(data.post.approved && data.post.active)) {
      //   this.router.navigate(['/']);
      // }
      this.postData.post = data.post;
      this.postExpired = moment().isAfter(data.post.post_end_time);
      this.postStartDate = moment(data.post.post_start_time).format('ll');
      this.postData.extra_fields = data.extra_fields;
      this.postData.images = data.images;
      this.postData.sellerInfo = data.seller_info;
      this.postData.sellerPrivacy.mobile = data.seller_privacy[0].mobile_public_disable;
      this.postData.sellerPrivacy.email = data.seller_privacy[0].email_public_disable;
      this.userRegisteredSince = moment(data.seller_info.created_at).format('ll');
      // tslint:disable-next-line:max-line-length
      this.sellerProfileImage = data.seller_info.profile_pic ? data.seller_info.profile_pic : './assets/images/detailpage/profileImageNotAvailable.jpeg';
      this.sellerRating = data.seller_info.rating === 0 ? null : String(Math.round(data.seller_info.rating));
      this.postData.qnS = data.QnA;
      this.loader = false;
      this.carJamAffUrl = 'https://carjam.co.nz/?partner=kwikz&plate=' + data.extra_fields.Plate_no_VIN;
      if (this.loggedIn) {
        this.role = +this.localStorage.getItem('used_id') === +this.postData.sellerInfo.id ? 'seller' : 'buyer';
        let watchlistArray = data.post.watcherslist.split(','); // checking watchList
        this.WatchListAlreadyAdded = (watchlistArray.indexOf(this.localStorage.getItem('used_id')) > -1);
      }
      if (data.seller_info.is_dealer && data.seller_info.dealer_business && data.seller_info.dealer_business.is_verified) {
        this.is_dealer = true;
        this.sellerProfileImage = data.seller_info.dealer_business.company_logo;
        let name = data.seller_info.dealer_business.name.split(' ');
        let routeName = name.join('-');
        this.dealer_route = '/car-dealer/' + data.seller_info.id + '/' + routeName + '/' + data.seller_info.dealer_business.id;
      }
      // tslint:disable-next-line:max-line-length
      let description = `${this.breadCrums.make} ${this.breadCrums.model} ${this.breadCrums.year} for Sale in ${this.breadCrums.city} ${this.breadCrums.region}, Price: $${data.post.amount} - Driven: ${data.extra_fields.kilometers_used} Kms - Color: ${data.extra_fields.exterior_color} - Transmission: ${data.extra_fields.transmission} - Fuel: ${data.extra_fields.fuel_type} | Kwikz`;
      this.meta.updateTag({
        name: 'description',
        content: description
      })
      // this.meta.addTags([
      //   {
      //     name: 'author', content: 'Kwikz'
      //   },
      //   {
      //     name: 'keywords', content: 'kwikz,used-car'
      //   },

      // ]);
    }, error => {
      this.loader = false;
      this.toasts.next({ text: 'Unable to fetch data, please try again', caption: 'Fail', type: 'danger' });
    });
    // console.log(this.postData);

    // loading image slider
    $('.main_slide').slick({
      dots: false,
      infinite: true,
      autoplay: true,
      autoplaySpeed: 3000,
      arrows: true,
      speed: 300,
      slidesToShow: 1,
      nextArrow: '<img src="./assets/images/detailpage/rightarrow.png" class="img-responsive serv_r"/>',
      prevArrow: '<img src="./assets/images/detailpage/leftarrow.png" class="img-responsive  serv_l"/>',
      adaptiveHeight: true
    });

  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }


  loginAndBack() {
    this.http.lastUrl.next(this.router.url);
    this.router.navigate(['/login']);
  }

  addToWatch() {
    if (!this.loggedIn) {
      if (confirm('Login is required to add post to watchList. Login now?')) {
        this.http.lastUrl.next(this.router.url);
        this.router.navigate(['/login']);
      }
    } else if (this.WatchListAlreadyAdded === true) {
      this.http.post('user/watchlistremove', { post_id: this.postId }).subscribe(data => {
        this.toasts.next({ text: 'Post removed from watchList', caption: 'Success', type: 'success' });
        this.ngOnInit();
      }, error => {
        let errorMsg = JSON.parse(error._body);
        console.log(errorMsg.msg);
        this.toasts.next({ text: errorMsg.msg, caption: 'Fail', type: 'danger' });
      }
      );
    } else {
      this.http.post('user/watchlistadd', { post_id: this.postId }).subscribe(data => {
        this.toasts.next({ text: 'Post added to watchList', caption: 'Success', type: 'success' });
        this.ngOnInit();
      }, error => {
        let errorMsg = JSON.parse(error._body);
        console.log(errorMsg.msg);
        this.toasts.next({ text: errorMsg.msg, caption: 'Fail', type: 'danger' });
      }
      );
    }

  }
  contactSellerFn() {
    if (this.contactSeller.message === '') {
      this.validationFailedContactSeller = true;
    } else {
      this.contactSeller.sellerEmail = this.postData.sellerInfo.email;
      this.contactSeller.sellerName = this.postData.sellerInfo.fname;
      this.contactSeller.buyerEmail = this.localStorage.getItem('email');
      this.contactSeller.buyerName = this.localStorage.getItem('fname');
      this.contactSeller.postUrl = window.location.origin + this.router.url;
      this.http.post('post/contactseller', this.contactSeller).subscribe(data => {
        this.toasts.next({ text: 'email to seller sent successfully', caption: 'Success', type: 'success' });
        this.contactSeller.price = '';
        this.contactSeller.mobile = '';
        this.contactSeller.message = '';
      }, error => {
        this.toasts.next({ text: 'Unable to send email to seller', caption: 'Fail', type: 'danger' });
      }
      );
    }

  }

  BreadCrumsFns(param) {
    switch (param) {

      case 'region':
        this.router.navigate([environment.filterUrl], { queryParams: { region: this.breadCrums.region } });
        break;

      case 'city':
        this.router.navigate([environment.filterUrl], { queryParams: { region: this.breadCrums.region, city: this.breadCrums.city } });
        break;

      case 'make':
        // tslint:disable-next-line:max-line-length
        this.router.navigate([environment.filterUrl], { queryParams: { region: this.breadCrums.region, city: this.breadCrums.city, make: this.breadCrums.make } });
        break;

      case 'model':
        // tslint:disable-next-line:max-line-length
        this.router.navigate([environment.filterUrl], { queryParams: { region: this.breadCrums.region, city: this.breadCrums.city, make: this.breadCrums.make, model: this.breadCrums.model } });
        break;

      case 'year':
        this.router.navigate([environment.filterUrl], { queryParams: this.breadCrums });
        break;

      default:
        break;
    }

  }

  answerUnansweredNow(i) {
    if (this.answerUnanswered[i] !== true) {
      this.answerUnanswered[i] = true;
    } else {
      this.answerUnanswered[i] = false;
    }
  }

  postQuestionFn() {
    if (this.postQuestion === '') {
      this.validationFailedPostQuestion = true;
    } else {
      this.http.post('post/question', { post_id: this.postId, question: this.postQuestion }).subscribe(success => {
        this.toasts.next({ text: 'Question added successfully', caption: 'Success', type: 'success' });
        this.postQuestion = '';
        this.ngOnInit();
      }, error => {
        this.toasts.next({ text: 'Unable to add question', caption: 'Fail', type: 'danger' });
      });
    }
  }

  postAnswerFn(questionId, i) {
    if (this.postAnswer === '') {
      this.validationFailedPostAnswer = true;
    } else {
      this.http.post('post/answer', { question_id: questionId, answer: this.postAnswer }).subscribe(success => {
        this.toasts.next({ text: 'Answer added successfully', caption: 'Success', type: 'success' });
        this.postAnswer = '';
        this.answerUnanswered[i] = !this.answerUnanswered[i];
        this.ngOnInit();
      }, error => {
        this.toasts.next({ text: 'Unable to add answer', caption: 'Fail', type: 'danger' });
      }
      );
    }
  }

  sendUserInviteUser() {
    // let inviteUserVal = $("#inviteUserTxt").val();
    // let chkEmailPattern;
    // chkEmailPattern = (/^(\s?[^\s,]+@[^\s,]+\.[^\s,]+\s?,)*(\s?[^\s,]+@[^\s,]+\.[^\s,]+)$/g .test(inviteUserVal));
    // if(chkEmailPattern === true){
    //   this.http.post(myGlobals.baseAPIPath + '/user/invite', {emails: inviteUserVal}, { headers: contentHeaders })
    //   .subscribe(
    //     response => {
    //       console.log('User Invited', response.json());
    //       if(response.json().status == 'ok'){
    //         this.inviteUser = '';
    //         alert('You have successfully sent out the invitations');
    //       }
    //     },
    //     error => {
    //         console.log(error.text());
    //     }
    //   );
    // }else{
    //   alert('Please enter valid email addresses');
    //   return false;
    // }
  }

  withdrawListing() {
    this.loader = true;
    this.http.post('post/withdraw', { post_id: this.postId }).subscribe(
      success => {
        this.loader = false;
        this.toasts.next({ text: 'Post withdrawn successfully', caption: 'Success', type: 'success' });
        this.ngOnInit();
      }, error => {
        this.loader = false;
        this.toasts.next({ text: 'Unable to withdraw post', caption: 'Fail', type: 'danger' });
      }
    );
  }

  markAsSold() {
    this.loader = true;
    this.http.post('post/marksold', { post_id: this.postId }).subscribe(
      success => {
        this.loader = false;
        this.toasts.next({ text: 'Post marked as sold', caption: 'Success', type: 'success' });
        this.ngOnInit();
      }, error => {
        this.loader = false;
        this.toasts.next({ text: 'Something went wrong, Please try again', caption: 'Fail', type: 'danger' });
      }
    );
  }

  reList() {
    this.loader = true;
    this.http.post('post/relist', { post_id: this.postId }).subscribe(
      success => {
        this.loader = false;
        this.toasts.next({ text: 'Post re-listed successfully', caption: 'Success', type: 'success' });
        this.ngOnInit();
      }, error => {
        this.loader = false;
        this.toasts.next({ text: 'Unable to re-list post', caption: 'Fail', type: 'danger' });
      }
    );
  }

}
