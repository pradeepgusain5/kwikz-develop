import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { StripeCheckoutLoader } from 'ng-stripe-checkout';
import { Router, ActivatedRoute } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { ToastNotifications } from 'ngx-toast-notifications';
import { DomSanitizer } from '@angular/platform-browser';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { environment } from '../../../../environments/environment';
import { SaleCarService, HttpServiceService } from '../../../_services';

@Component({
  selector: 'app-sell-car',
  templateUrl: './sell-car.component.html',
  styleUrls: ['./sell-car.component.scss']
})
export class SellCarComponent implements OnInit {
  bucket: any = new S3({
    accessKeyId: environment.accessKeyId,
    secretAccessKey: environment.secretAccessKey,
    region: environment.region
  });
  carPostUrl: string = environment.carPostUrl;
  feesAmount: number = environment.amount;
  currency: string = environment.currency;
  // stripeCheckoutHandler: StripeCheckoutHandler;
  currentPage = 1;
  mode = 'new'; // new or update
  updatePostID: number;  // used for edit/update post
  plateNumber: string;
  newCar: any = {};
  makes: any = [];
  models: any = [];
  years: any = [];
  isNotValidateOne = false;
  errMsg: any = [];
  // tslint:disable-next-line:max-line-length
  cities: any = ['Northland', 'Auckland', 'Waikato', 'Bay of Plenty', 'Gisborne', 'Hawke\'s Bay', 'Taranaki', 'Whanganui', 'Manawatu', 'International', 'Wairarapa', 'Wellington', 'Nelson Bays', 'Marlborough', 'West Coast', 'Canterbury', 'Timaru - Oamaru', 'Otago', 'Southland', 'North Island', 'South Island', 'Other'];
  bodyStyles: any = ['Classic', 'Convertible', 'Coupe', 'Hatchback', 'Minivans', 'Sedan', 'Station Wagon', 'RV/SUV', 'Utility', 'Van'];
  kwikzFuelTypes: any = ['Other', 'Petrol', 'Diesel', 'Hybrid', 'Electric'];
  fuelTypes: any = [];
  doors: any = ['Don\'t Know', '1', '2', '3', '4', '5'];
  // tslint:disable-next-line:max-line-length
  cylinders: any = ['Don\'t Know', 'Rotary', '3 Cylinder', '4 Cylinder', '5 Cylinder', '6 Cylinder', '8 Cylinder', '10 Cylinder', '12 Cylinder'];
  transmissionTypes: any = ['Automatic', 'Manual', 'Tiptronic', 'Others'];
  importHistories: any = ['Don\'t Know', 'NZ NEW', 'Imported'];
  registrationExpires: any = ['Don\'t Know', 'No Rego'];
  wofExpires: any = ['Don\'t Know', 'No WOF'];
  listingDurations: any = ['7 days', '14 days'];
  noOfOwners: any = ['Don\'t Know', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '10+'];
  noOfSeats: any = ['Don\'t Know', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '12+'];
  secondCall = true;

  // google geolocation user setting (Object)
  public autoCompleteSettings: any = {
    showRecentSearch: false,
    geoCountryRestriction: ['nz'],
    searchIconUrl: 'http://downloadicons.net/sites/default/files/identification-search-magnifying-glass-icon-73159.png',
    geoTypes: ['(regions)', '(cities)']
  };

  // Image Upload Starts
  files: File[];
  fileKeys: any = [];
  recentlySelected: any = [];

  public loader: boolean;
  // Image Upload Ends
  makeFormatter = (data: any) => `<span>${data.make}</span>`;
  modelFormatter = (data: any) => `<span>${data.model}</span>`;
  yearFormatter = (data: any) => `<span>${data.year}</span>`;

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    public _DomSanitizationService: DomSanitizer,
    public saleCarService: SaleCarService,
    public stripeCheckoutLoader: StripeCheckoutLoader,
    private toasts: ToastNotifications,
    private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    public meta: Meta,
    public title: Title
  ) {
    this.route.params.subscribe(params => {
      // tslint:disable-next-line:triple-equals
      this.mode = params.id == 'new' ? 'new' : 'update';
      this.updatePostID = params.id;  // incase of edit/update
      this.localStorage.setItem('car_id', params.id);
    });

    // Image Uploads Start
    this.files = []; // local uploading files array
    this.fileKeys = [];
    this.newCar.images = [];
    // Image Uploads End
  }


  ngOnInit() {
    this.title.setTitle('Sell Used Car Online: Selling Your Car is Going to be Smooth Ride | Kwikz');
    this.meta.updateTag({
      name: 'description', content: 'Are You Looking to Selling Your Car Online? At Kwikz - Upload up to 20 Photos, No Hidden Cost, Direct Communication, Relist Until Sold.  List Your Car Free!'
    })
    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,sell-car'
    //   },

    // ]);

    // check if loggedIn user is owner/authorized to edit post
    // tslint:disable-next-line:triple-equals
    if (this.mode == 'update') {
      this.loader = true;
      let data = {
        post_id: this.updatePostID
      };
      this.http.post('post/checkowner', data).subscribe(response => {
        if (response.check) {
          this.getCar(); // fetching data if edit/update post
        }
      }, error => {
        console.log('Not authorized to update this post');
        this.router.navigate(['/']);
      }
      );
    }


    // secondary calls
    let date = new Date();
    const currentYear = date.getFullYear()
    this.getMakes();
    this.setFuelType();
    this.registrationExpires = this.saleCarService.getMultyMonths((currentYear - 1), (currentYear + 2), this.registrationExpires);
    this.wofExpires = this.saleCarService.getMultyMonths(currentYear, (currentYear + 3), this.wofExpires);
    this.scrollUpToTop();
  }

  getCar() {
    // if mode id update(edit) then fetch car data from api and create an object
    // let carId = localStorage.getItem('car_id');
    // tslint:disable-next-line:triple-equals
    let carId = this.mode == 'new' ? this.localStorage.getItem('car_id') : this.updatePostID;
    // tslint:disable-next-line:triple-equals
    if (carId && (carId != 'new')) {
      this.saleCarService.getCar(carId).subscribe(response => {
        let a = response.post;
        this.newCar = response.extra_fields;
        this.newCar.location = a.location;
        this.newCar.region = a.region;
        this.newCar.city = a.city;
        this.newCar.id = a.id;
        this.newCar.desc = a.desc;
        this.newCar.amount = a.amount;
        this.newCar.near_offer = a.near_offer;
        this.newCar.images = response.images;
        this.currentPage = 1; // for edit post, landing page will be 1

        this.autoCompleteSettings['inputString'] = this.newCar.location; // updating geolocation from carDetails(edit mode)
        this.autoCompleteSettings = Object.assign({}, this.autoCompleteSettings);  // Very Important Line to add after modifying settings.
        this.loader = false;
      },
        error => {
          this.toasts.next({ text: 'Unable to fetch saved car detail from API', caption: 'Fail', type: 'danger' });
          this.loader = false;
        });
    }
  }

  getCarDetails() {  // fetching from carJam
    this.loader = true;
    let payload = {
      Plate_no_VIN: this.plateNumber
    };
    this.saleCarService.checkDuplicatePost(payload).subscribe(
      data => {
        let postExpire = false;
        let todayDate = new Date();
        let postDate = new Date(data.post_end_date);
        if (todayDate > postDate) {
          postExpire = true;
        }
        if (data.duplicate && !postExpire) {
          this.loader = false;
          this.toasts.next({ text: 'Duplicate Plate Number. Vehicle is already in database!', caption: 'Fail', type: 'danger' });
        } else {
          this.saleCarService.getCarDetails(this.plateNumber).subscribe(response => {
            this.loader = false;
            // tslint:disable-next-line:triple-equals
            if (response.scode != 'err-plate-is-invalid') {
              this.secondCall = true;
              this.newCar.Plate_no_VIN = response.plate;
              this.newCar.make = response.make;
              this.newCar.model = response.model;
              this.newCar.model_detail = response.submodel;
              this.newCar.engine_size = response.cc_rating;
              this.newCar.year_manufacture = response.year_of_manufacture;
              this.newCar.kilometers_used = response.latest_odometer_reading;
              this.newCar.exterior_color = response.main_colour;
              // this.newCar.body_style = response.submodel; carJam don't have
              // tslint:disable-next-line:radix
              this.newCar.fuel_type = this.fuelTypes[parseInt(response.fuel_type)];

              // tslint:disable-next-line:radix
              this.newCar.no_of_owners = this.noOfOwners[parseInt(response.number_of_owners)];
              // tslint:disable-next-line:radix
              this.newCar.no_of_seats = this.noOfOwners[parseInt(response.no_of_seats)];
              this.newCar.registration_exp = this.saleCarService.timeConverter(response.licence_expiry_date, 0);
              this.newCar.transmission = response.transmission;
              this.newCar.Wof_expires = this.saleCarService.timeConverter(response.expiry_date_of_last_successful_wof, 1);
              this.importHistoryValue(response.previous_country_of_registration);

            } else {
              if (this.secondCall) {
                this.secondCall = false;
                setTimeout(() => { this.getCarDetails(); }, 3000);
              } else {
                this.loader = false;
                // tslint:disable-next-line:max-line-length
                this.toasts.next({ text: 'Number plate entered is INVALID. Please enter a valid plate number.', caption: 'Fail', type: 'danger' });
              }
            }
          }, error => {
            if (this.secondCall) {
              this.secondCall = false;
              setTimeout(() => { this.getCarDetails(); }, 3000);
            } else {
              this.loader = false;
              this.toasts.next({ text: 'Number plate entered is INVALID', caption: 'Fail', type: 'danger' });
            }
          });
        }
      }, error => {
        this.loader = false;
        this.toasts.next({ text: 'Number plate entered is INVALID', caption: 'Fail', type: 'danger' });
      }
    );
  }

  isValidateOne() {
    this.errMsg = [];
    this.isNotValidateOne = false;
    if (!this.newCar.make) {
      this.isNotValidateOne = true;
      this.errMsg['make'] = 'Make is required field.';
    }
    if (!this.newCar.model) {
      this.isNotValidateOne = true;
      this.errMsg['model'] = 'Model is required field.';
    }
    if (!this.newCar.year_manufacture) {
      this.isNotValidateOne = true;
      this.errMsg['year_of_manufacture'] = 'Year is required field.';
    }
    if (!this.newCar.kilometers_used) {
      this.isNotValidateOne = true;
      this.errMsg['latest_odometer_reading'] = 'Kilo metre is required field.';
    }
    if (!this.newCar.exterior_color) {
      this.isNotValidateOne = true;
      this.errMsg['main_colour'] = 'Exterior Colour is required field.';
    }
    if (!this.newCar.body_style) {
      this.isNotValidateOne = true;
      this.errMsg['body_style'] = 'Body Style is required field.';
    }
    if (!this.newCar.fuel_type) {
      this.isNotValidateOne = true;
      this.errMsg['fuel_type'] = 'Fuel Type is required field.';
    }
    if (!this.newCar.Plate_no_VIN) {
      this.isNotValidateOne = true;
      this.errMsg['plate'] = 'Number Plate is required field.';
    }

    if (this.isNotValidateOne) {
      this.scrollUpToTop();
    } else {
      if (!this.newCar.id) {  // checking if ID exist or not
        this.storeCar();   // create new post (new entry)
      } else {
        this.updateCar(2);  // updating post (edit/update mode)
      }
      this.scrollUpToTop();
    }
  }

  storeCar() {
    this.loader = true;
    this.saleCarService.storeCar(this.newCar).subscribe(response => {
      this.loader = false;
      // tslint:disable-next-line:triple-equals
      if (response.status == 'ok') {
        this.newCar.id = response.post_id;
        this.localStorage.setItem('car_id', this.newCar.id);
        this.currentPage = 2;
      } else {
        this.currentPage = 1;
        // tslint:disable-next-line:max-line-length
        this.toasts.next({ text: 'Sorry ! Something went wrong. Please try again.', caption: 'Fail', type: 'danger' }); // alert('Sorry ! Car Store API Failed.');
      }
    });
  }

  updateCar(goToPage) {
    this.loader = true;
    this.saleCarService.updateCar(this.newCar).subscribe(response => {
      this.loader = false;
      // console.log(response);
      this.toasts.next({ text: 'Post updated successfully', caption: 'Success', type: 'success' });
      // tslint:disable-next-line:triple-equals
      if (response.status == 'ok') {
        this.currentPage = goToPage;
      } else {
        this.currentPage = goToPage - 1;
        // tslint:disable-next-line:max-line-length
        this.toasts.next({ text: 'Sorry ! Something went wrong. Please try again.', caption: 'Fail', type: 'danger' }); // alert('Sorry ! Car Update API Failed.');
      }
    }, error => {
      this.loader = false;
      this.toasts.next({ text: 'Unable to update post', caption: 'Fail', type: 'danger' });
      console.log('Unable to update post');
    });
  }

  isValidateTwo() {
    this.errMsg = [];
    this.isNotValidateOne = false;
    if (!this.newCar.amount || (this.newCar.amount < this.newCar.min_amount)) {
      this.isNotValidateOne = true;
      this.errMsg['amount'] = 'Asking Amount is required and should be more than Minimum Value field.';
    }
    // if (!this.newCar.near_offer) {
    // 	this.isNotValidateOne = true;
    // 	this.errMsg['near_offer'] = "Near Offer is required field.";
    // }
    if (!this.newCar.location) {
      this.isNotValidateOne = true;
      this.errMsg['location'] = 'Location is required field.';
    }
    if (this.isNotValidateOne) {
      this.scrollUpToTop();
    } else {
      this.updateCar(3);
      this.scrollUpToTop();
    }
  }

  // address add & update
  autoCompleteCallback1(eventGoogle) {
    let city = 'New Zealand';
    let region = 'New Zealand';
    eventGoogle.data.address_components.forEach(element => {
      // tslint:disable-next-line:triple-equals
      if (element.types[0] == 'locality') {
        city = element.long_name;
      }
      // tslint:disable-next-line:triple-equals
      if (element.types[0] == 'administrative_area_level_1') {
        region = element.long_name;
      }
      // tslint:disable-next-line: triple-equals
      if (city == 'New Zealand') {
        city = region;
      }
    });

    this.newCar.location = eventGoogle.data.description;
    this.newCar.city = city;
    this.newCar.region = region;
    // let formattedAddr = this.extractAddress(eventGoogle.data.address_components);
  }

  onRoadChange(section) {
    switch (section) {
      case 'exclude':
        this.newCar.onroad_costexcluded = 1;
        this.newCar.onroad_costincluded = 0;
        break;

      default:
        this.newCar.onroad_costexcluded = 0;
        this.newCar.onroad_costincluded = 1;
        break;
    }
  }

  // stripeInit() {
  // 	this.stripeCheckoutLoader.createHandler({
  //         key: environment.stripeKey,
  //         token: (token) => {
  //             // Do something with the token...
  //             console.log('Payment successful!', token);
  //             localStorage.removeItem('car_id');
  //             this.newCar = [];
  //             // this.currentPage = 1;
  //         }
  //     }).then((handler: StripeCheckoutHandler) => {
  //         this.stripeCheckoutHandler = handler;
  //     });
  // }

  changePage(pageNo) {
    if (this.newCar.id) {
      this.currentPage = pageNo;
    } else {
      this.toasts.next({ text: 'Please fill the required details first.', caption: 'Fail', type: 'danger' });
    }
  }

  checkStep(step, goTo) {
    this.currentPage = goTo;
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    this.recentlySelected = [];
    this.files = [];
  }

  importHistoryValue(val) {
    this.newCar.import_history = this.importHistories[0];
    if (val === '') {
      this.newCar.import_history = this.importHistories[1];
    } else {
      if (val) {
        this.newCar.import_history = this.importHistories[2];
      }
    }
  }

  getMakes() {
    this.saleCarService.getMakes().subscribe(response => {
      if (response.result) {
        this.makes = response.result;
      } else {
        this.toasts.next({ text: 'Sorry ! Something went wrong. Please try again.', caption: 'Fail', type: 'danger' }); // api failed
      }
    },
      error => {
        // alert(error.text());
        console.log(error.text());
      });
  }

  getModel(makeObj) {
    this.newCar.make_id = makeObj.make_id;
    this.saleCarService.getModels(this.newCar.make_id).subscribe(response => {
      if (response.result) {
        this.models = response.result;
        if (this.newCar.model) {
          for (let i = 0; i < this.models.length; ++i) {
            // tslint:disable-next-line:triple-equals
            if (this.newCar.model.toLowerCase() == this.models[i]['model'].toLowerCase()) {
              this.newCar.model = this.models[i]['model'];
            }
          }
        }
      } else {
        // tslint:disable-next-line:max-line-length
        this.toasts.next({ text: 'Sorry ! Something went wrong. Please try again.', caption: 'Fail', type: 'danger' }); // alert('Sorry ! Car Models API Failed.');
      }
    },
      error => {
        // alert(error.text());
        console.log(error.text());
      });
  }

  getYears(modelObj) {
    this.newCar.model_id = modelObj.model_id;
    this.saleCarService.getYears(this.newCar.make_id, this.newCar.model_id).subscribe(response => {
      if (response.result) {
        this.years = response.result;
        if (this.newCar.year_of_manufacture) {
          for (let i = 0; i < this.years.length; ++i) {
            // tslint:disable-next-line:triple-equals
            if (this.newCar.year_of_manufacture.toLowerCase() == this.years[i]['year'].toLowerCase()) {
              this.newCar.year_of_manufacture = this.years[i]['year'];
            }
          }
        }
      } else {
        // tslint:disable-next-line:max-line-length
        this.toasts.next({ text: 'Sorry ! Something went wrong. Please try again.', caption: 'Fail', type: 'danger' }); // alert('Sorry ! Car Models API Failed.');
      }
    },
      error => {
        // alert(error.text());
        console.log(error.text());
      });
  }

  setFuelType() {
    this.fuelTypes = [];
    this.fuelTypes[0] = 'Other';
    this.fuelTypes[1] = 'Petrol';
    this.fuelTypes[2] = 'Diesel';
    this.fuelTypes[3] = 'Other'; // 'CNG'
    this.fuelTypes[4] = 'Other'; // 'LPG'
    this.fuelTypes[5] = 'Electric';
    this.fuelTypes[6] = 'Other';
    this.fuelTypes[7] = 'Hybrid'; // 'Petrol Hybrid';
    this.fuelTypes[8] = 'Hybrid'; // 'Diesel Hybrid';
    this.fuelTypes[9] = 'Hybrid'; // 'Petrol Electric Hybrid';
    this.fuelTypes[10] = 'Hybrid'; // 'Diesel electric Hybrid';
    this.fuelTypes[11] = 'Hybrid'; // 'Plug-in petrol hybrid';
    this.fuelTypes[12] = 'Hybrid'; // 'Plug-in diesel hybrid';
    this.fuelTypes[13] = 'Electric'; // 'Electric - petrol extended';
    this.fuelTypes[14] = 'Electric'; // 'Electric - diesel extended';
    this.fuelTypes[15] = 'Electric'; // 'Electric hydrogen fuel cell';
    this.fuelTypes[16] = 'Electric'; // 'Electric other fuel cell';
    this.fuelTypes[91] = 'Hybrid'; // 'Petrol Hybrid';
    this.fuelTypes[92] = 'Hybrid'; // 'Plug-in Hybrid-Petrol';
    this.fuelTypes[93] = 'Hybrid'; // 'Hybrid';
    this.fuelTypes[94] = 'Hybrid'; // 'Diesel Hybrid';
    this.fuelTypes[95] = 'Hybrid'; // 'Electric Hybrid';
    this.fuelTypes[96] = 'Hybrid'; // 'Plug-in Hybrid-Diesel';
    this.fuelTypes[97] = 'Electric'; // 'Electric (fuel cell)';
  }

  // Image Upload Starts Functions
  onDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
  }

  setImages(evt, isDroped) {
    evt.preventDefault();
    this.recentlySelected = [];
    if (isDroped) {
      this.files = evt.dataTransfer.files;
    } else {
      this.files = evt.target.files;
    }
    let file = this.files[0];
    if (this.files && file) {
      // We iterate the array in the code
      for (let i = 0; i < this.files.length; i++) {
        let type = this.files[i].type;
        let reader = new FileReader();
        // reader.onload = e => this.recentlySelected.push(reader.result);
        reader.readAsDataURL(this.files[i]);
        this.recentlySelected.push(window.URL.createObjectURL(this.files[i]));
      }
    }
  }

  openFileInput() {
    document.getElementById('uploadImageInput').click();
  }

  saveImages() {
    for (let file of this.files) {
      this.loader = true;
      let params = {
        Bucket: 'bhaskar-kwikz',
        Key: 'post/car/' + this.newCar.id + '/' + Date.now() + '-' + Math.floor(Math.random() * (+9999999 - +100)) + +100 + '-' + file.name,
        Body: file,
        ACL: 'public-read',
        StorageClass: 'REDUCED_REDUNDANCY'
      };

      this.bucket.upload(params, (err, data) => {
        if (err) {
          console.log('There was an error uploading your file: ', err);
          return false;
        }
        this.newCar.images.push(data['Location']);
        this.saleCarService.uploadImages(this.newCar.images, this.newCar.id).subscribe(
          response => {
            this.loader = false;
            this.recentlySelected = [];
            this.toasts.next({ text: 'Image Uploaded successfully.', caption: 'Success', type: 'success' });
            this.checkStep(3, 4);
          },
          error => {
            this.loader = false;
            this.toasts.next({ text: 'Unable to upload image, please try again.', caption: 'Fail', type: 'danger' });
          });
        // console.log('Image Uploaded successfully: ', data);
      });
    }
  }

  deleteImage(index, path) {
    this.loader = true;
    this.newCar.images.splice(index, 1);
    this.saleCarService.uploadImages(this.newCar.images, this.newCar.id).subscribe(
      response => {
        this.loader = false;
        this.recentlySelected = [];
        this.toasts.next({ text: 'Image Deleted successfully.', caption: 'Success', type: 'success' });

      },
      error => {
        this.loader = false;
        this.toasts.next({ text: 'Unable to delete image, please try again.', caption: 'Fail', type: 'danger' });
      });
  }

  deleteSelectedImage(index) {
    console.log(this.files, index);
    this.files.splice(index, 1);
    this.recentlySelected.splice(index, 1);
  }
  // Image Upload Ends Functions

  scrollUpToId(id) {
    let el = document.getElementById(id);
    el.scrollIntoView({ behavior: 'smooth' });
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  // payment
  // onClickBuy() {
  //     this.stripeCheckoutHandler.open({
  //         amount: environment.feesAmount,
  //         currency: environment.currency,
  //     });
  // }

  // public onClickCancel() {
  //     // If the window has been opened, this is how you can close it:
  //     this.stripeCheckoutHandler.close();
  // }
  onClickBuy() {
    // validation of imp fields (amount, location)
    if (this.newCar.location === '') {
      this.toasts.next({ text: 'Location missing, please add location in step 2', caption: 'Fail', type: 'danger' });
      return;
    }

    // payment gateway success


    // making post approve & active
    this.http.post('post/postactive', { post_id: this.newCar.id }).subscribe(success => { }, error => {
      this.toasts.next({ text: 'Unable to make post active', caption: 'Fail', type: 'danger' });
    }
    );


    // transaction api
    let transData = {
      post_id: this.newCar.id,
      currency: this.currency,
      amount: this.feesAmount,
      transaction_id: 'trans_promo_launch', // get it from payment gateway
      payment_channel: 'launch_promo',
      payment_status: 'captured',
      description: 'NA'
    };
    this.http.post('transaction', transData).subscribe(success => { }, error => {
      this.toasts.next({ text: 'Unable to post transaction entry', caption: 'Fail', type: 'danger' });
    });

    // removing post_id from localStorage
    this.localStorage.removeItem('car_id');
    // success toast msg
    this.toasts.next({ text: 'Post is active', caption: 'Success', type: 'success' });

    // routing to post listing page
    if (this.newCar.region === this.newCar.city) {
      // tslint:disable-next-line:max-line-length
      this.router.navigate([this.carPostUrl + '/' + this.newCar.region.toLowerCase() + '/' + this.newCar.make.toLowerCase() + '/' + this.newCar.model.toLowerCase() + '/' + this.newCar.year_manufacture + '/' + this.newCar.id]);
    } else {
      // tslint:disable-next-line:max-line-length
      this.router.navigate([this.carPostUrl + '/' + this.newCar.region.toLowerCase() + '-' + this.newCar.city.toLowerCase() + '/' + this.newCar.make.toLowerCase() + '/' + this.newCar.model.toLowerCase() + '/' + this.newCar.year_manufacture + '/' + this.newCar.id]);
    }

  }
}
