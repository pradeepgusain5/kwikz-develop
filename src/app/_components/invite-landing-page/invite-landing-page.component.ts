import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http } from '@angular/http';
import { HttpServiceService, InvitationService } from '../../_services';
import { ToastNotifications } from 'ngx-toast-notifications';
import { ITopInvitees } from '../../_interfaces'; import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Inject } from '@angular/core';
declare var $: any;
declare var gapi: any;
declare var FB: any;

@Component({
  selector: 'app-invite-landing-page',
  templateUrl: './invite-landing-page.component.html',
  styleUrls: ['./invite-landing-page.component.scss']
})
export class InviteLandingPageComponent implements OnInit {

  authConfig: any = {};
  googleContacts: any = [];
  userFilter: any = { id: '' };
  customerCredit: any;
  public topInviteesUser: ITopInvitees[] = [];
  public referralId: string;
  public signUpWithReferralId: string;
  public whatsAppReferral: string;
  public showCopied = false;
  public showImportedContact: any = 0;

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private http: HttpServiceService,
    // tslint:disable-next-line: deprecation
    private httpClient: Http,
    public toasts: ToastNotifications,
    private invitationService: InvitationService
  ) { }

  ngOnInit() {
    this.authConfig = {
      client_id: environment.google_client_id,
      scope: 'https://www.googleapis.com/auth/contacts.readonly'
    };
    this.getTopInvitees();
    this.scrollUpToTop();
    this.getReferralId();
  }

  private getReferralId() {
    this.referralId = this.localStorage.getItem('referral_id');
    this.signUpWithReferralId = `www.kwikz.nz/signup?referral=${this.referralId}`;
    this.whatsAppReferral = `https://api.whatsapp.com/send?text=www.kwikz.nz/signup?referral%3D${this.referralId}`;
  }

  public copyToClipBoard(val) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);

    this.showCopied = true;
    setTimeout(() => {
      this.showCopied = false;
    }, 1000);
  }

  private scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  private getTopInvitees() {
    this.invitationService.getTopInvitees().subscribe(res => {
      let response = res.data.filter(e => e.invited_confirmed > 0);
      this.topInviteesUser = response.slice(0, 3);
    });
  }

  googleSignIn() {
    gapi.client.setApiKey(environment.google_api_key);
    gapi.auth2.authorize(this.authConfig, this.handleAuthorization);
  }

  // tslint:disable-next-line: arrow-parens
  handleAuthorization = (authorizationResult) => {
    if (authorizationResult && !authorizationResult.error) {
      let url: string = 'https://www.google.com/m8/feeds/contacts/default/thin?' +
        'alt=json&max-results=500&v=3.0&access_token=' +
        authorizationResult.access_token;
      this.httpClient.get(url).subscribe(
        response => {
          this.fetchContacts(response);
        }
      );
    }
  }

  fetchContacts(data) {
    this.googleContacts = [];
    let googleData = JSON.parse(data._body).feed.entry;
    for (let email of googleData) {
      let mail = {
        id: email.gd$email[0].address
      };
      this.googleContacts.push(mail);
    }
  }

  sendInvitation() {
    let inviteUsersPostData = {
      emails: []
    };
    for (let contact of this.googleContacts) {
      if (contact.share) {
        inviteUsersPostData.emails.push(contact.id);
      }
    }
    this.sendInvitationMail(inviteUsersPostData);
  }

  sendInvitationMail(mail) {
    this.showImportedContact = mail.emails.length;
    this.http.post('user/invite', mail).subscribe(
      response => {
        this.toasts.next({ text: 'Invited successfully!', caption: 'Success', type: 'success' });
        setTimeout(() => {
          this.showImportedContact = false;
        }, 5000);
      }, error => {
        this.toasts.next({ text: 'All emails are already invited', caption: 'Fail', type: 'danger' });
        setTimeout(() => {
          this.showImportedContact = false;
        }, 5000);
      }
    );
  }

  selectAllContact() {
    for (let contact of this.googleContacts) {
      contact.share = true;
    }
  }

  shareOnFB() {
    FB.ui({
      method: 'share',
      href: this.signUpWithReferralId,
      // tslint:disable-next-line:space-before-function-paren
    }, function (response) { });
  }

  shareOnTwitter() {
    // let text = encodeURIComponent('Hey everyone, come & see how good I look!');
    let shareUrl = 'https://twitter.com/intent/tweet?text=' + this.signUpWithReferralId;
    let win = window.open(shareUrl, 'ShareOnTwitter', this.getWindowOptions());
    win.opener = null;
  }

  // tslint:disable-next-line:space-before-function-paren
  getWindowOptions = function () {
    let width = 500;
    let height = 350;
    let left = (window.innerWidth / 2) - (width / 2);
    let top = (window.innerHeight / 2) - (height / 2);

    return [
      'resizable,scrollbars,status',
      'height=' + height,
      'width=' + width,
      'left=' + left,
      'top=' + top,
    ].join();
  };

  inviteUserCustom() {
    let inviteUsersPostData = {
      emails: []
    };
    inviteUsersPostData.emails.push(this.customerCredit);
    if (this.customerCredit === '') {
      return; // validation
    }
    this.http.post('user/invite', inviteUsersPostData).subscribe(
      response => {
        this.customerCredit = '';
        this.toasts.next({ text: 'You have invited users successfully', caption: 'Success', type: 'success' });
      },
      error => {
        this.customerCredit = '';
        this.toasts.next({ text: 'Unable to invite user', caption: 'Fail', type: 'danger' });
      }
    );
  }
}
