import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { StripeCheckoutLoader } from 'ng-stripe-checkout';
import { Router, ActivatedRoute } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { ToastNotifications } from 'ngx-toast-notifications';
import { DomSanitizer } from '@angular/platform-browser';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { MatChipInputEvent } from '@angular/material';
import { environment } from '../../../environments/environment';
import { COMMA, ENTER, SPACE } from '@angular/cdk/keycodes';
import { SaleCarService, HttpServiceService } from '../../_services';

@Component({
  selector: 'app-sell-multi-car',
  templateUrl: './sell-multi-car.component.html',
  styleUrls: ['./sell-multi-car.component.scss']
})
export class SellMultiCarComponent implements OnInit {

  bucket: any = new S3({
    accessKeyId: environment.accessKeyId,
    secretAccessKey: environment.secretAccessKey,
    region: environment.region
  });
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, SPACE];
  public emailList;
  submitted: boolean;
  carPostUrl: string = environment.carPostUrl;
  feesAmount: number = environment.amount;
  currency: string = environment.currency;
  currentPage = 1;
  mode = 'new';
  updatePostID: number;
  plateNumber: string;
  newCar: any = {};
  makes: any = [];
  models: any = [];
  years: any = [];
  isNotValidateOne = true;
  errMsg: any = [];
  // tslint:disable-next-line:max-line-length
  cities: any = ['Northland', 'Auckland', 'Waikato', 'Bay of Plenty', 'Gisborne', 'Hawke\'s Bay', 'Taranaki', 'Whanganui', 'Manawatu', 'International', 'Wairarapa', 'Wellington', 'Nelson Bays', 'Marlborough', 'West Coast', 'Canterbury', 'Timaru - Oamaru', 'Otago', 'Southland', 'North Island', 'South Island', 'Other'];
  bodyStyles: any = ['Classic', 'Convertible', 'Coupe', 'Hatchback', 'Minivans', 'Sedan', 'Station Wagon', 'RV/SUV', 'Utility', 'Van'];
  kwikzFuelTypes: any = ['Other', 'Petrol', 'Diesel', 'Hybrid', 'Electric'];
  fuelTypes: any = [];
  doors: any = ['Don\'t Know', '1', '2', '3', '4', '5'];
  secondCall = true;
  searchTags: any = [];
  listOfCars: any = [];
  // Image Upload Starts
  files: File[];
  fileKeys: any = [];
  recentlySelected: any = [];
  // tslint:disable-next-line:max-line-length
  cylinders: any = ['Don\'t Know', 'Rotary', '3 Cylinder', '4 Cylinder', '5 Cylinder', '6 Cylinder', '8 Cylinder', '10 Cylinder', '12 Cylinder'];
  transmissionTypes: any = ['Automatic', 'Manual', 'Tiptronic', 'Others'];
  importHistories: any = ['Don\'t Know', 'NZ NEW', 'Imported'];
  registrationExpires: any = ['Don\'t Know', 'No Rego'];
  wofExpires: any = ['Don\'t Know', 'No WOF'];
  listingDurations: any = ['7 days', '14 days'];
  noOfOwners: any = ['Don\'t Know', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '10+'];
  noOfSeats: any = ['Don\'t Know', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '12+'];
  public autoCompleteSettings: any = {
    showRecentSearch: false,
    geoCountryRestriction: ['nz'],
    searchIconUrl: 'http://downloadicons.net/sites/default/files/identification-search-magnifying-glass-icon-73159.png',
    geoTypes: ['(regions)', '(cities)']
  };
  public loader: boolean;
  makeFormatter = (data: any) => `<span>${data.make}</span>`;
  modelFormatter = (data: any) => `<span>${data.model}</span>`;
  yearFormatter = (data: any) => `<span>${data.year}</span>`;

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    public _DomSanitizationService: DomSanitizer,
    public saleCarService: SaleCarService,
    public stripeCheckoutLoader: StripeCheckoutLoader,
    private toasts: ToastNotifications,
    private http: HttpServiceService,
    private router: Router,
    private route: ActivatedRoute,
    public meta: Meta,
    public title: Title
  ) {
    this.setFuelType();
    let date = new Date();
    const currentYear = date.getFullYear()
    this.registrationExpires = this.saleCarService.getMultyMonths((currentYear - 1), (currentYear + 2), this.registrationExpires);
    this.wofExpires = this.saleCarService.getMultyMonths(currentYear, (currentYear + 3), this.wofExpires);
    this.route.params.subscribe(params => {
      this.mode = params.id === 'new' ? 'new' : 'update';
      this.updatePostID = params.id;
      this.localStorage.setItem('car_id', params.id);
    });

    // Image Uploads Start
    this.files = []; // local uploading files array
    this.fileKeys = [];
    // Image Uploads End
  }

  ngOnInit() {
    this.title.setTitle('Sell Used Car Online: Selling Your Car is Going to be Smooth Ride | Kwikz');
    this.meta.updateTag({
      name: 'description', content: 'Are You Looking to Selling Your Car Online? At Kwikz - Upload up to 20 Photos, No Hidden Cost, Direct Communication, Relist Until Sold.  List Your Car Free!'
    })
    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Bhaskar Rajoriya'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz'
    //   },

    // ]);
    this.loader = true;
    this.getMakes();
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  addNewSearchTag() {
    let newTag = { plate: '' };
    this.searchTags.push(newTag);
  }

  addTag(event) {
    if (event.value.trim() !== '') {
      this.searchTags.push(event.value.trim());
      event.input.value = '';
    }
  }

  onRemoveTag(tag: any) {
    let index = this.searchTags.indexOf(tag, 0);
    if (index > -1) {
      this.searchTags.splice(index, 1);
    }
  }

  autoCompleteCallback1(eventGoogle, newCar) {
    let city = 'New Zealand';
    let region = 'New Zealand';
    eventGoogle.data.address_components.forEach(element => {
      // tslint:disable-next-line:triple-equals
      if (element.types[0] == 'locality') {
        city = element.long_name;
      }
      // tslint:disable-next-line:triple-equals
      if (element.types[0] == 'administrative_area_level_1') {
        region = element.long_name;
      }
      if (city === 'New Zealand') {
        city = region;
      }
    });
    newCar.location = eventGoogle.data.description;
    newCar.city = city;
    newCar.region = region;
  }

  onRoadChange(section, newCar) {
    switch (section) {
      case 'exclude':
        newCar.onroad_costexcluded = 1;
        newCar.onroad_costincluded = 0;
        break;
      default:
        newCar.onroad_costexcluded = 0;
        newCar.onroad_costincluded = 1;
        break;
    }
  }

  importHistoryValue(val, newCar) {
    newCar.import_history = this.importHistories[0];
    if (val === '') {
      newCar.import_history = this.importHistories[1];
    } else {
      if (val) {
        newCar.import_history = this.importHistories[2];
      }
    }
  }

  getCarDetails() {
    this.listOfCars = [];
    for (let tag of this.searchTags) {
      let newCar = {};
      this.loader = true;
      let payload = {
        Plate_no_VIN: tag
      };
      this.saleCarService.checkDuplicatePost(payload).subscribe(
        data => {
          let postExpire = false;
          let todayDate = new Date();
          let postDate = new Date(data.post_end_date);
          if (todayDate > postDate) {
            postExpire = true;
          }
          if (data.duplicate && !postExpire) {
            // duplicate entry
            this.loader = false;
            this.toasts.next({ text: 'Duplicate Plate Number. Vehicle is already in database!', caption: 'Fail', type: 'danger' });
          } else {
            this.saleCarService.getCarDetails(tag).subscribe(response => {
              this.loader = false;
              // tslint:disable-next-line:triple-equals
              if (response.scode != 'err-plate-is-invalid') {
                newCar['Plate_no_VIN'] = response.plate;
                newCar['make'] = response.make;
                newCar['model'] = response.model;
                newCar['model_detail'] = response.submodel;
                newCar['engine_size'] = response.cc_rating;
                newCar['year_manufacture'] = response.year_of_manufacture;
                newCar['kilometers_used'] = response.latest_odometer_reading;
                newCar['exterior_color'] = response.main_colour;
                newCar['fuel_type'] = this.fuelTypes[+response.fuel_type];
                newCar['no_of_owners'] = this.noOfOwners[+response.number_of_owners];
                newCar['no_of_seats'] = this.noOfOwners[+response.no_of_seats];
                newCar['registration_exp'] = this.saleCarService.timeConverter(response.licence_expiry_date, 0);
                newCar['transmission'] = response.transmission;
                newCar['Wof_expires'] = this.saleCarService.timeConverter(response.expiry_date_of_last_successful_wof, 1);
                this.importHistoryValue(response.previous_country_of_registration, newCar);
                newCar['images'] = [];
                this.listOfCars.push(newCar);
              } else {
                this.loader = false;
                // tslint:disable-next-line:max-line-length
                this.toasts.next({ text: 'Number plate entered is INVALID. Please enter a valid plate number.', caption: 'Fail', type: 'danger' });
              }
            }, error => {
              this.loader = false;
              this.toasts.next({ text: 'Number plate entered is INVALID', caption: 'Fail', type: 'danger' });
            });
          }
        }
      );
    }
    this.searchTags = [];
  }

  getMakes() {
    this.saleCarService.getMakes().subscribe(response => {
      if (response.result) {
        this.makes = response.result;
      } else {
        this.toasts.next({ text: 'Sorry ! Something went wrong. Please try again.', caption: 'Fail', type: 'danger' }); // api failed
      }
    }, error => {
      console.log(error.text());
    });
    this.loader = false;
  }

  getModel(makeObj, newCar) {
    newCar.make_id = makeObj.make_id;
    this.saleCarService.getModels(newCar.make_id).subscribe(response => {
      if (response.result) {
        this.models = response.result;
        if (newCar.model) {
          for (let i = 0; i < this.models.length; ++i) {
            // tslint:disable-next-line:triple-equals
            if (newCar.model.toLowerCase() == this.models[i]['model'].toLowerCase()) {
              newCar.model = this.models[i]['model'];
            }
          }
        }
      } else {
        // tslint:disable-next-line:max-line-length
        this.toasts.next({ text: 'Sorry ! Something went wrong. Please try again.', caption: 'Fail', type: 'danger' }); // alert('Sorry ! Car Models API Failed.');
      }
    }, error => {
      console.log(error.text());
    });
  }

  getYears(modelObj, newCar) {
    newCar.model_id = modelObj.model_id;
    this.saleCarService.getYears(newCar.make_id, newCar.model_id).subscribe(response => {
      if (response.result) {
        this.years = response.result;
        if (newCar.year_of_manufacture) {
          for (let i = 0; i < this.years.length; ++i) {
            // tslint:disable-next-line:triple-equals
            if (newCar.year_of_manufacture.toLowerCase() == this.years[i]['year'].toLowerCase()) {
              newCar.year_of_manufacture = this.years[i]['year'];
            }
          }
        }
      } else {
        // tslint:disable-next-line:max-line-length
        this.toasts.next({ text: 'Sorry ! Something went wrong. Please try again.', caption: 'Fail', type: 'danger' }); // alert('Sorry ! Car Models API Failed.');
      }
    }, error => {
      console.log(error.text());
    });
  }

  isValidateOne(newCar) {
    this.errMsg = [];
    this.isNotValidateOne = false;
    if (!newCar.make) {
      this.isNotValidateOne = true;
      this.errMsg['make'] = 'Make is required field.';
    }
    if (!newCar.model) {
      this.isNotValidateOne = true;
      this.errMsg['model'] = 'Model is required field.';
    }
    if (!newCar.year_manufacture) {
      this.isNotValidateOne = true;
      this.errMsg['year_of_manufacture'] = 'Year is required field.';
    }
    if (!newCar.kilometers_used) {
      this.isNotValidateOne = true;
      this.errMsg['latest_odometer_reading'] = 'Kilo metre is required field.';
    }
    if (!newCar.exterior_color) {
      this.isNotValidateOne = true;
      this.errMsg['main_colour'] = 'Exterior Colour is required field.';
    }
    if (!newCar.body_style) {
      this.isNotValidateOne = true;
      this.errMsg['body_style'] = 'Body Style is required field.';
    }
    if (!newCar.fuel_type) {
      this.isNotValidateOne = true;
      this.errMsg['fuel_type'] = 'Fuel Type is required field.';
    }
    if (!newCar.Plate_no_VIN) {
      this.isNotValidateOne = true;
      this.errMsg['plate'] = 'Number Plate is required field.';
    }
    if (!newCar.amount || (newCar.amount < newCar.min_amount)) {
      this.isNotValidateOne = true;
      this.errMsg['amount'] = 'Asking Amount is required and should be more than Minimum Value field.';
    }
    if (!newCar.location) {
      this.isNotValidateOne = true;
      this.errMsg['location'] = 'Location is required field.';
    }
    if (!newCar.transmission) {
      this.isNotValidateOne = true;
      this.errMsg['transmission'] = 'Transmission is required field.';
    }
    if (newCar.images && newCar.images.length === 0) {
      this.isNotValidateOne = true;
      this.errMsg['images'] = 'Images are required field.';
    }
    if (this.isNotValidateOne) {
      this.scrollUpToTop();
    } else {
      // this.storeCar(newCar);
      // this.scrollUpToTop();
    }
  }

  validateForm() {
    for (let newCar of this.listOfCars) {
      this.isValidateOne(newCar);
    }
  }

  submit() {
    this.loader = true;
    let i = 0;
    for (let newCar of this.listOfCars) {
      newCar.engine_size = newCar.engine_size + '';
      i++;
      this.storeCar(newCar);
      this.loader = false;
      if (i === this.listOfCars.length) {
        this.submitted = true;
        this.loader = false;
      }
    }
  }

  storeCar(newCar) {
    this.saleCarService.storeCarInOneStep(newCar).subscribe(response => {
      if (response.status === 'ok') {
        newCar.id = response.post_id;
        this.localStorage.setItem('car_id', newCar.id);
      } else {
        this.currentPage = 1;
        // tslint:disable-next-line:max-line-length
        this.toasts.next({ text: 'Sorry ! Something went wrong. Please try again.', caption: 'Fail', type: 'danger' }); // alert('Sorry ! Car Store API Failed.');
      }
    }, error => {
      console.log('Unable to create post', error);
    });
  }

  // Image Upload Starts Functions
  onDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
  }

  setImages(evt, isDroped, newCar) {
    evt.preventDefault();
    newCar.recentlySelected = [];
    if (isDroped) {
      newCar.files = evt.dataTransfer.files;
    } else {
      newCar.files = evt.target.files;
    }
    let file = newCar.files[0];
    if (newCar.files && file) {
      // We iterate the array in the code
      for (let i = 0; i < newCar.files.length; i++) {
        let type = newCar.files[i].type;
        let reader = new FileReader();
        reader.readAsDataURL(newCar.files[i]);
        newCar.recentlySelected.push(window.URL.createObjectURL(newCar.files[i]));
      }
      this.saveImages(newCar);
    }
  }

  openFileInput() {
    document.getElementById('uploadImageInput').click();
  }

  saveImages(newCar) {
    for (let file of newCar.files) {
      this.loader = true;
      let params = {
        Bucket: 'bhaskar-kwikz',
        Key: 'post/car/' + Date.now() + '-' + Math.floor(Math.random() * (+9999999 - +100)) + +100 + '-' + file.name,
        Body: file,
        ACL: 'public-read',
        StorageClass: 'REDUCED_REDUNDANCY'
      };

      this.bucket.upload(params, (err, data) => {
        if (err) {
          console.log('There was an error uploading your file: ', err);
          return false;
        }
        newCar.images.push(data['Location']);
        this.loader = false;
        // this.saleCarService.uploadImages(newCar.images, newCar.id).subscribe(
        //   response => {
        //     this.loader = false;
        //     this.recentlySelected = [];
        //     this.toasts.next({ text: 'Image Uploaded successfully.', caption: 'Success', type: 'success' });
        //   },
        //   error => {
        //     this.loader = false;
        //     this.toasts.next({ text: 'Unable to upload image, please try again.', caption: 'Fail', type: 'danger' });
        //   });
      });
    }
  }

  showDetail(newCar) {
    if (newCar.show) {
      newCar.show = false;
    } else {
      for (let newCarObj of this.listOfCars) {
        newCarObj.show = false;
      }
      newCar.show = true;
    }
  }

  deleteImage(index, path, newCar) {
    this.loader = true;
    newCar.images.splice(index, 1);
    // this.saleCarService.uploadImages(newCar.images, newCar.id).subscribe(
    //   response => {
    //     this.loader = false;
    //     this.recentlySelected = [];
    //     this.toasts.next({ text: 'Image Deleted successfully.', caption: 'Success', type: 'success' });

    //   },
    //   error => {
    //     this.loader = false;
    //     this.toasts.next({ text: 'Unable to delete image, please try again.', caption: 'Fail', type: 'danger' });
    //   });
  }

  deleteSelectedImage(index) {
    this.files.splice(index, 1);
    this.recentlySelected.splice(index, 1);
  }
  // Image Upload Ends Functions

  scrollUpToId(id) {
    let el = document.getElementById(id);
    el.scrollIntoView({ behavior: 'smooth' });
  }

  setFuelType() {
    this.fuelTypes = [];
    this.fuelTypes[0] = 'Other';
    this.fuelTypes[1] = 'Petrol';
    this.fuelTypes[2] = 'Diesel';
    this.fuelTypes[3] = 'Other'; // 'CNG'
    this.fuelTypes[4] = 'Other'; // 'LPG'
    this.fuelTypes[5] = 'Electric';
    this.fuelTypes[6] = 'Other';
    this.fuelTypes[7] = 'Hybrid'; // 'Petrol Hybrid';
    this.fuelTypes[8] = 'Hybrid'; // 'Diesel Hybrid';
    this.fuelTypes[9] = 'Hybrid'; // 'Petrol Electric Hybrid';
    this.fuelTypes[10] = 'Hybrid'; // 'Diesel electric Hybrid';
    this.fuelTypes[11] = 'Hybrid'; // 'Plug-in petrol hybrid';
    this.fuelTypes[12] = 'Hybrid'; // 'Plug-in diesel hybrid';
    this.fuelTypes[13] = 'Electric'; // 'Electric - petrol extended';
    this.fuelTypes[14] = 'Electric'; // 'Electric - diesel extended';
    this.fuelTypes[15] = 'Electric'; // 'Electric hydrogen fuel cell';
    this.fuelTypes[16] = 'Electric'; // 'Electric other fuel cell';
    this.fuelTypes[91] = 'Hybrid'; // 'Petrol Hybrid';
    this.fuelTypes[92] = 'Hybrid'; // 'Plug-in Hybrid-Petrol';
    this.fuelTypes[93] = 'Hybrid'; // 'Hybrid';
    this.fuelTypes[94] = 'Hybrid'; // 'Diesel Hybrid';
    this.fuelTypes[95] = 'Hybrid'; // 'Electric Hybrid';
    this.fuelTypes[96] = 'Hybrid'; // 'Plug-in Hybrid-Diesel';
    this.fuelTypes[97] = 'Electric'; // 'Electric (fuel cell)';
  }
}
