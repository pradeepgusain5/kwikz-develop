import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { HttpServiceService } from '../../_services/http-service.service';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'app-recentposts',
    templateUrl: './recentposts.component.html',
    styleUrls: ['./recentposts.component.scss']
})
// tslint:disable-next-line:component-class-suffix
export class RecentPosts implements OnInit {
    public recentPostData: [];
    public categoryData = [];
    public blogBaseUrl: string = environment.apiBlog;

    constructor(private http: HttpServiceService) { }

    ngOnInit() {
        this.loadRecentPosts();
        this.loadCategories();
    }

    loadRecentPosts() {
        this.http.getBlog('blogs?_sort=createdAt:DESC&_limit=5').subscribe(
            data => {
                this.recentPostData = data;
            }, error => {
                console.log(error);
            }
        );
    }
    loadCategories() {
        this.http.getBlog('categories').subscribe(
            data => {
                data.forEach(element => {
                    this.categoryData.push(element.CategoryName);
                });
                // console.log(this.categoryData);

            }, error => {
                console.log(error);
            }
        );
    }

}
