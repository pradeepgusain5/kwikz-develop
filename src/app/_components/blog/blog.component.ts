import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { HttpServiceService } from '../../_services/http-service.service';
import { environment } from '../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';
import { UrlSlugify } from '../../_pipes/url-slugify.pipe';
@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent implements OnInit {
  public blogData: [];
  public blogBaseUrl: string = environment.apiBlog;
  category = ''
  blogId: any;
  paginatedData = false
  noOfPages = []
  lastPage = 1
  currentPage = 1
  TEN = 5

  constructor(public meta: Meta, public title: Title,
    private http: HttpServiceService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.route.params.subscribe(
      params => {
        if (params.category) {
          this.category = params.category.split('-').join(' ');
          this.loadBlogsWithData(1);
          this.loadMetaForCategory();
          this.scrollUpToTop();
        } else {
          this.category = ''
          this.loadBlogs(1);
          this.title.setTitle('Kwikz Blog: Online Blog for Selling & Buying Cars Online | Kwikz');
          this.meta.updateTag({
            name: 'description', content: 'Kwikz Blog: Online Blog for Selling & Buying Car Online. Kwikz helps you in finding and selling a vehicle. Read more...'
          })
          this.scrollUpToTop();
        }
      }
    );
  }

  ngOnInit() {
    // this.title.setTitle('Kwikz Blog: Online Blog for Selling & Buying Cars Online | Kwikz');
    // this.meta.updateTag({
    //   // tslint:disable-next-line:max-line-length
    //   name: 'description', content: 'Kwikz is a product designed by the vision to provide a smooth car buying and selling to New Zealanders. Zero success fee & re-list until sold. Signup!'
    // })
    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,blog'
    //   },

    // ]);
    this.getTotalCount()
    this.scrollUpToTop();
  }

  getTotalCount() {
    if (this.category == '') {
      this.http.getBlog('blogs/count').subscribe(
        data => {
          // console.log('count no data', data)
          if (data > this.TEN) {
            this.lastPage = Math.ceil(data / this.TEN);
            for (let i = 1; i <= +this.lastPage; i++) {
              this.noOfPages.push(i);
            }
            this.paginatedData = true
          } else {
            this.paginatedData = false
          }
        }, error => {
          console.log(error);
        }
      );
    } else {
      this.http.getBlog(`/blogs/count?category.CategoryName_contains=${this.category}`).subscribe(
        data => {
          if (!data) {
            this.router.navigate(['blogs'])
          }
          if (data > this.TEN) {
            this.lastPage = Math.ceil(data / this.TEN);
            for (let i = 1; i <= +this.lastPage; i++) {
              this.noOfPages.push(i);
            }
            this.paginatedData = true
          } else {
            this.paginatedData = false
          }
        }, error => {
          console.log(error);
        }
      );
    }
  }

  loadBlogs(page) {
    let skipPage = (page - 1) * this.TEN
    this.http.getBlog(`blogs?_sort=createdAt:DESC&_start=${skipPage}&_limit=${this.TEN}`).subscribe(
      data => {
        this.blogData = data;
      }, error => {
        console.log(error);
      }
    );
  }
  loadBlogsWithData(page) {
    // console.log('working with data')
    let skipPage = (page - 1) * this.TEN
    this.http.getBlog(`blogs?category.CategoryName_contains=${this.category}&_sort=createdAt:DESC&_start=${skipPage}&_limit=${this.TEN}`).subscribe(
      data => {
        this.blogData = data;
      }, error => {
        console.log(error);
      }
    );
  }
  loadNextPagePosts(page) {
    if (this.category == '') {
      this.loadBlogs(page)
    } else {
      this.loadBlogsWithData(page)
    }
    this.scrollUpToTop()
    this.currentPage = page
  }
  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  shareOnTweet(postId, postTitle) {
    let postSlug = UrlSlugify.prototype.transform({ key: postTitle, id: postId })
    let shareLink = `Hey%20Guys!%20check%20this%20blog%20I%20found%20https%3A%2F%2Fwww.kwikz.nz${postSlug}`
    let twitter = document.createElement('a');
    twitter.style.display = 'none';
    // twitter.setAttribute('id', 'twShare');
    twitter.setAttribute('href', `https://twitter.com/intent/tweet?text=${shareLink}`);
    twitter.setAttribute('target', '_blank');
    twitter.click()
  }
  shareOnFb(postId, postTitle) {
    let postSlug = UrlSlugify.prototype.transform({ key: postTitle, id: postId })
    let appId = environment.fbAppId
    let link = `http%3A%2F%2Fwww.kwikz.nz${postSlug}`
    let shareLink = `https://www.facebook.com/dialog/share?app_id=${appId}&display=popup&href=${link}`
    let fb = document.createElement('a');
    fb.style.display = 'none';
    // fb.setAttribute('id', 'fbShare');
    fb.setAttribute('target', '_blank');
    fb.setAttribute('href', shareLink);
    fb.click()
  }

  loadMetaForCategory() {
    this.http.getBlog(`categories?CategoryName_contains=${this.category}&_start=0&_limit=1`).subscribe(
      data => {
        if (data[0] && data[0].pageTitle) {
          this.title.setTitle(data[0].pageTitle);
        } else {
          this.title.setTitle('Kwikz Blog: Online Blog for Selling & Buying Cars Online | Kwikz');
        }
        if (data[0] && data[0].pageDescription) {
          this.meta.updateTag({
            name: 'description', content: data[0].pageDescription
          })
        } else {
          this.meta.updateTag({
            name: 'description', content: 'Kwikz Blog: Online Blog for Selling & Buying Car Online. Kwikz helps you in finding and selling a vehicle. Read more...'
          })
        }
        // this.metaData = data;
      }, error => {
        console.log(error);
      }
    );
  }
}
