import { Component, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { HttpServiceService } from '../../_services/http-service.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-blogpost',
  templateUrl: './blogdetail.component.html',
  styleUrls: ['./blogdetail.component.scss']
})

export class BlogDetailComponent implements OnInit, OnChanges {
  public postId: number;
  public postSlug: String;
  public PostData: any;
  public blogBaseUrl: string = environment.apiBlog;
  public dataLoaded = false;

  constructor(
    public meta: Meta,
    public title: Title,
    private http: HttpServiceService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.route.params.subscribe(
      params => {
        if (params) {
          // this.postId = params.id;
          this.postSlug = params.title
          this.ngOnInit();
          this.scrollUpToTop();
        } else {
          this.goToBlog()
        }
      }
    );
  }

  ngOnInit() {

    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,blog'
    //   },

    // ]);
    this.scrollUpToTop();
    let searchParam = this.postSlug.split('-').join(' ')
    this.http.getBlog(`/blogs?Title_contains=${searchParam}`).subscribe(
      data => {
        if (data) {
          this.PostData = data[0];
          this.dataLoaded = true;
          if (this.PostData.PageTitle) {
            this.title.setTitle(this.PostData.PageTitle);
          }
          if (this.PostData.PageDescription) {
            this.meta.updateTag({
              // tslint:disable-next-line:max-line-length
              name: 'description', content: this.PostData.PageDescription
            })
          }
        } else {
          this.goToBlog()
        }
      }, error => {
        this.goToBlog()
      }
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    // console.log('changes found');
  }

  loadBlog() {

  }

  goToBlog() {
    this.router.navigate(['/blogs'])
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  shareOnTweet() {
    let shareLink = `Hey%20Guys!%20check%20this%20blog%20I%20found%20https%3A%2F%2Fwww.kwikz.nz%2Fblog%2F${this.postSlug}`
    let twitter = document.createElement('a');
    twitter.style.display = 'none';
    // twitter.setAttribute('id', 'twShare');
    twitter.setAttribute('href', `https://twitter.com/intent/tweet?text=${shareLink}`);
    twitter.setAttribute('target', '_blank');
    twitter.click()
    // document.getElementById("twShare").remove();
  }
  shareOnFb() {
    let appId = environment.fbAppId
    // let link = `localhost:4200/blog/${this.postSlug}/${this.postId}`
    let link = `https%3A%2F%2Fwww.kwikz.nz%2Fblog%2F${this.postSlug}`
    let shareLink = `https://www.facebook.com/dialog/share?app_id=${appId}&display=popup&href=${link}`
    let fb = document.createElement('a');
    fb.style.display = 'none';
    // fb.setAttribute('id', 'fbShare');
    fb.setAttribute('target', '_blank');
    fb.setAttribute('href', shareLink);
    fb.click()
    // document.getElementById("fbShare").remove();
  }
}
