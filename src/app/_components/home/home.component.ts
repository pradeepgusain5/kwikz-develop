import { WINDOW } from '@ng-toolkit/universal';
import { Meta, Title } from '@angular/platform-browser';
import { environment } from '../../../environments/environment';
import { Component, OnInit, ElementRef, AfterViewInit, Inject } from '@angular/core';
import { ToastNotifications } from 'ngx-toast-notifications';
import { bool } from 'aws-sdk/clients/signer';
import { HttpServiceService } from '../../_services';
import { Router } from '@angular/router';
import { NguCarouselConfig } from '@ngu/carousel';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  filterUrl: string = environment.filterUrl;
  feedData: object[] = [];
  feedDataLoaded = false;
  carPostUrl: string = environment.carPostUrl;
  loggedIn: boolean;
  carFeedSkeleton: any = [1, 2, 3, 4, 5];
  public carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 1, md: 3, lg: 3, all: 0 },
    slide: 1,
    speed: 500,
    point: {
      visible: true
    },
    load: 5,
    velocity: 0,
    touch: true,
    easing: 'cubic-bezier(0, 0, 0.2, 1)',
    interval: {
      timing: 10000,
      initialDelay: 1000
    },
    loop: true,
  };

  constructor(
    @Inject(WINDOW) private window: Window,
    private router: Router,
    public meta: Meta,
    public title: Title,
    private http: HttpServiceService,
    private toasts: ToastNotifications,
    private el: ElementRef
  ) {
  }

  ngAfterViewInit() {
  }

  ngOnInit() {
    this.title.setTitle('Kwikz: Find Used Cars, Buy & Sell Used Cars Online in New Zealand');
    this.meta.updateTag({
      // tslint:disable-next-line:max-line-length
      name: 'description', content: 'Thinking of Selling a Car? At Kwikz, Buy & Sell Used Cars Online in New Zealand. Find Used Cars, Search By Easy Filter Options.  Sign Up & List Car for Free!'
    })
    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,home'
    //   },

    // ]);

    this.getCarFeed();

    this.http.isLoggedFn().subscribe(
      x => {
        this.loggedIn = x;
      }
    );

    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  getCarFeed() {
    this.http.get('car/feeds').subscribe(
      data => {
        this.feedData = data;
        this.feedDataLoaded = true;
      }, error => {
        this.toasts.next({ text: 'Unable to fetch feeds', caption: 'Fail', type: 'danger' });
      }
    );
  }

  openOptions() {
    this.window.document.getElementById('dashboard-button').click();
  }

  checkLogin() {
    this.router.navigate(['/business-account']);
    // if (this.loggedIn) {
    //   if (localStorage.getItem('is_dealer') === 'true') {
    //     this.router.navigate(['/myaccount']);
    //   } else {
    //     this.router.navigate(['/dealer/register']);
    //   }
    // } else {
    //   if (localStorage.getItem('is_dealer') === 'true') {
    //     this.http.lastUrl.next('/myaccount');
    //   } else {
    //     this.http.lastUrl.next('/dealer/register');
    //   }
    //   this.router.navigate(['/login']);
    // }
  }
}
