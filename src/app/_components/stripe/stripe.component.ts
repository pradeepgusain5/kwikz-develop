import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-stripe',
  templateUrl: './stripe.component.html',
  styleUrls: ['./stripe.component.scss']
})
export class StripeComponent implements OnInit {

  cardNumber: string;
  expiryMonth: string;
  expiryYear: string;
  cvc: string;
  message: string;
  customeStripe: any;

  constructor(private _zone: NgZone, public activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activeRoute.queryParams
      .subscribe(params => {
        this.customeStripe = params.custome;
      });
  }

  openCheckout() {
    let handler = (window as any).StripeCheckout.configure({
      key: 'pk_test_oi0sKPJYLGjdvOXOM8tE8cMa',
      locale: 'auto',
      token(token: any) {
        alert(token.id);
        // Get the token ID to your server-side code for use.
      }
    });
    handler.open({
      name: 'Demo Stripe',
      description: 'Description of payment process',
      amount: 2000    // Amount payable is multiplied by 100 e.g. payable amount is 20 but pass the value 20*100 = 2000
    });
  }

  getToken() {
    this.message = 'Loading...';
    (window as any).Stripe.card.createToken({
      number: this.cardNumber,
      exp_month: this.expiryMonth,
      exp_year: this.expiryYear,
      cvc: this.cvc
    }, (status: number, response: any) => {
      this._zone.run(() => {
        if (status === 200) {
          this.message = `Success!`;
          alert(response.card.id);
        } else {
          this.message = response.error.message;
        }
      });
    });
  }

}
