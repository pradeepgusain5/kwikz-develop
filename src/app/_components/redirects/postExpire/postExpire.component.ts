import { Component, OnInit } from '@angular/core';
// import { Meta, Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpServiceService } from '../../../_services/http-service.service';
// import {environment} from '../../../environments/environment';


@Component({
  selector: 'app-postExpire',
  templateUrl: './postExpire.component.html',
  styleUrls: ['./postExpire.component.scss']
})
export class PostExpireComponent implements OnInit {


  constructor(private http: HttpServiceService, private route: ActivatedRoute, private router: Router) {
    this.route.params.subscribe(
      params => {
        console.log(params.id);
      }
    );
  }

  ngOnInit() {
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

}
