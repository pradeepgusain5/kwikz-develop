import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastNotifications } from 'ngx-toast-notifications';
import { AuthenticationService, HttpServiceService } from '../../../_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loggedIn: boolean;
  registerForm: FormGroup;
  submitted = false;
  passwordType = 'password';
  passwordShow = false;
  public loader: boolean;

  ngOnInit() {

    this.title.setTitle('Login: Sell Your Car & Find Your Car | Kwikz');
    this.meta.updateTag({
      // tslint:disable-next-line:max-line-length
      name: 'description', content: 'Kwikz helps sellers to advertise their car online. Buying and selling cars is Quick and Hassle-free. To Buy & Sell Used Car in New Zealand - Login here.'
    })
    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,login'
    //   },

    // ]);
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]]
    });

    this.http.isLoggedFn().subscribe(
      x => {
        this.loggedIn = x;
        if (this.loggedIn) {
          this.router.navigate(['/']);
        }
      }
    );
    this.scrollUpToTop();
  }

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private router: Router,
    private http: HttpServiceService,
    private auth: AuthenticationService,
    private formBuilder: FormBuilder,
    private toasts: ToastNotifications,
    public meta: Meta,
    public title: Title
  ) {
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      console.log('validation failed');
      return;    // stop here if form is invalid
    }
    this.loader = true;
    this.auth.login(this.registerForm.value).subscribe(
      res => {
        this.localStorage.setItem('token', res.data.token);
        this.localStorage.setItem('used_id', res.user_id);
        this.http.loggedIn.next(this.localStorage.getItem('token') ? true : false);
        this.loader = false;
        if (this.http.lastUrl.value === '') { // route if there exist last URL
          this.router.navigate(['/']);
        } else {
          this.router.navigate([this.http.lastUrl.value]);
          this.http.lastUrl.next('');
        }
        if (this.localStorage.getItem('token')) {
          this.getProfile();
        }
      }, error => {
        this.loader = false;
        this.toasts.next({ text: 'Unable to login, please check your email/password', caption: 'Fail', type: 'danger' });
      });
  }

  getProfile() {
    this.auth.getProfile()
      .subscribe(
        res => {
          this.localStorage.setItem('fname', res.user_detail.fname);
          this.localStorage.setItem('email', res.user_detail.email);
          this.localStorage.setItem('mobile', res.user_detail.mobile);
          this.localStorage.setItem('is_dealer', res.user_detail.is_dealer);
        }, error => console.log(error)
      );
  }

  togglePasswordType() {
    this.passwordShow = !this.passwordShow;
    this.passwordType = this.passwordShow ? 'text' : 'password';

  }
  signup(event) {
    event.preventDefault();
    this.router.navigate(['signup']);
  }

}
