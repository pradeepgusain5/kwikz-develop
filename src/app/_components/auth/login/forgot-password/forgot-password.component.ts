import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../../_services/authentication.service';
import { ToastNotifications } from 'ngx-toast-notifications';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  emailID = '';
  validationCheck = false;
  public loader: boolean;

  constructor(
    private auth: AuthenticationService,
    private toasts: ToastNotifications
  ) { }

  ngOnInit() {
    this.scrollUpToTop();
  }

  forgotPassword() {
    if (this.emailID === '') {
      this.validationCheck = true;
      return;
    } else {
      this.loader = true;
      return this.auth.sendPasswordResetEmail(this.emailID).subscribe(
        success => {
          this.loader = false;
          this.validationCheck = false;
          this.emailID = '';
          this.toasts.next({ text: 'Forgot email sent successfully', caption: 'Success', type: 'success' });
        },
        error => {
          this.loader = false;
          this.validationCheck = false;
          this.emailID = '';
          this.toasts.next({ text: 'Unable to reset, either email  invalid or incorrect ', caption: 'Fail', type: 'danger' });
        }
      );
    }
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }
}
