import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../../_services/authentication.service';
import { ToastNotifications } from 'ngx-toast-notifications';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  passwordsMatch = false;
  // tslint:disable-next-line:variable-name
  confirm_password: string;
  password: string;
  warning: string;
  token: string;
  // tslint:disable-next-line:variable-name
  user_id: any;
  validationCheck = false;
  public loader: boolean;

  constructor(
    public authenticationService: AuthenticationService,
    public router: Router,
    public route: ActivatedRoute,
    private toasts: ToastNotifications
  ) {
    this.route.params.subscribe(params => {
      this.token = params['token'];
    });
  }

  ngOnInit() {
    this.loader = true;
    this.authenticationService.verifyTokenForResetPassword(this.token).subscribe(
      data => {
        this.user_id = data.user;
        this.loader = false;
      }, error => {
        this.loader = false;
        this.toasts.next({ text: 'Something went wrong, Please try again.', caption: 'Fail', type: 'danger' });
        setTimeout(() => {
          this.router.navigate(['/login']);
        }, 3000);
      }
    );
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  onChangePassword() {
    if (!this.password || this.password.length < 6) {
      this.validationCheck = true;
      return;
    } else if (this.password !== this.confirm_password) {
      this.passwordsMatch = true;
      return;
    } else {
      this.validationCheck = false;
      this.passwordsMatch = false;
      // tslint:disable-next-line:variable-name
      let user_detail = {
        user_id: this.user_id,
        token: this.token,
        password: this.password
      };
      this.authenticationService.resetUserPassword(user_detail)
        .subscribe(result => {
          this.toasts.next({ text: 'Password change successfully.', caption: 'Success', type: 'success' });
          setTimeout(() => {
            this.router.navigate(['/login']);
          }, 3000);
        }, error => {
          this.toasts.next({ text: 'Something went wrong, Please try again.', caption: 'Fail', type: 'danger' });
          setTimeout(() => {
            this.router.navigate(['/login']);
          }, 3000);
        });
    }
  }

}
