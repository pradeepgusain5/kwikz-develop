import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Meta, Title } from '@angular/platform-browser';
import { ToastNotifications } from 'ngx-toast-notifications';
import { AuthenticationService } from '../../../_services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  submitted = false;
  acceptTerm = false;
  passwordType = 'password';
  passwordShow = false;
  promo: any = '';
  public loader: boolean;

  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    private auth: AuthenticationService,
    private toasts: ToastNotifications,
    public meta: Meta,
    public title: Title,
    private activatedRoute: ActivatedRoute
  ) { }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return; // stop here if form is invalid
    }
    if (!this.acceptTerm) {
      return;
    }
    this.loader = true;
    this.registerForm.value.mobile = '+64' + this.registerForm.value.mobile;
    this.auth.register(this.registerForm.value).subscribe(
      res => {
        this.loader = false;
        this.toasts.next({ text: 'Successfully registered, Please verify your email', caption: 'Success', type: 'success' });
        setTimeout(() => {
          this.router.navigate(['/']);
        }, 3000);
      }, error => {
        this.loader = false;
        if (error.status === 422) {
          this.toasts.next({ text: 'Email already registered', caption: 'Fail', type: 'danger' });
        } else {
          this.toasts.next({ text: 'Unable to register, Please try again', caption: 'Fail', type: 'danger' });
        }
      }
    );
  }


  login(event) {
    event.preventDefault();
    this.router.navigate(['login']);
  }


  ngOnInit() {
    this.title.setTitle('Signup: List Car for Free, Sell Used Cars & Find Your Dream Car | Kwikz');
    this.meta.updateTag({
      // tslint:disable-next-line:max-line-length
      name: 'description', content: 'Kwikz Benefits: Zero Success Fee, List Car for Free, Relist Your Car Untill it is Sold, Quick & Easy Process to Find Your Dream Car. Signup Today! '
    })
    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,signup,register'
    //   },
    // ]);
    this.registerForm = this.formBuilder.group({
      fname: ['', [Validators.required]],
      lname: ['', [Validators.required]],
      mobile: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      promo: ['']
    });
    this.activatedRoute.queryParams.subscribe(
      (params: Params) => {
        if (JSON.stringify(params) !== '{}') {
          this.promo = params.referral;
        }
      }
    );
    this.registerForm.get('promo').setValue(this.promo);
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  togglePasswordType() {
    this.passwordShow = !this.passwordShow;
    this.passwordType = this.passwordShow ? 'text' : 'password';
  }

}
