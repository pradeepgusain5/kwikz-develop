import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../_services/authentication.service';
import { Router } from '@angular/router';
import { HttpServiceService } from '../../_services/http-service.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  loggedIn = false;
  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    public authService: AuthenticationService,
    public router: Router,
    private http: HttpServiceService
  ) { }

  ngOnInit() {
    this.http.isLoggedFn().subscribe(
      x => {
        this.loggedIn = x;
      }
    );
  }

  toggleNavigation() {
    $('#collapsibleNavbar').slideToggle(500);
  }

  upNavigation() {
    $('#collapsibleNavbar').slideUp(0);
  }

  logout() {
    this.localStorage.clear();
    this.http.loggedIn.next(this.localStorage.getItem('token') ? true : false);
    this.router.navigate(['/login']);
  }
}
