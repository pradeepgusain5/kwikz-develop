import { Component, Input } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-multi-item',
  templateUrl: './multi-item.component.html',
  styleUrls: ['./multi-item.component.scss']
})
export class MultiItemComponent {

  public carouselItems: Array<any> = [];
  @Input() carFeed;
  carPostUrl: string = environment.carPostUrl;
  carNoImage: string = environment.CarNoImage;
  // carFeedImages: any;

  public carouselTile: NguCarouselConfig = {
    grid: { xs: 1, sm: 1, md: 3, lg: 3, all: 0 },
    slide: 1,
    speed: 500,
    point: {
      visible: true
    },
    load: 5,
    velocity: 0,
    touch: true,
    easing: 'cubic-bezier(0, 0, 0.2, 1)',
    interval: {
      timing: 10000,
      initialDelay: 1000
    },
    loop: true,
  };

  constructor() { }

}
