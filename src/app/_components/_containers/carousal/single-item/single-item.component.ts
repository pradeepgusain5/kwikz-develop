import { Component, Input } from '@angular/core';
import { environment } from '../../../../../environments/environment.prod';

@Component({
  selector: 'app-single-item',
  templateUrl: './single-item.component.html',
  styleUrls: ['./single-item.component.scss']
})
export class SingleItemComponent {

  public carouselItems: Array<any> = [];
  @Input() carFeed;
  carPostUrl: string = environment.carPostUrl;

  public carousal = {
    noWrapSlides: false,
    showIndicator: false,
    myInterval: 3000,
    activeSlideIndex: 0,
    noPauseIndicator: true
  };

  constructor() { }

}
