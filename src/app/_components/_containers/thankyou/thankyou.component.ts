import { Component, OnInit, Inject, PLATFORM_ID } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastNotifications } from 'ngx-toast-notifications';
import { HttpServiceService } from '../../../_services';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss']
})
export class ThankyouComponent implements OnInit {

  token = '';
  isVerified = false;
  text = 'Please Wait ! We are verifying the account.';
  isBrowser: boolean;
  public loader: boolean;

  constructor(
    public toasts: ToastNotifications,
    public http: HttpServiceService,
    public router: Router,
    public route: ActivatedRoute,
    @Inject(PLATFORM_ID) private platformId: any,
  ) {
    this.route.params.subscribe(params => {
      this.token = params.token;
    });
  }

  ngOnInit() {
    if (this.token) {
      if (isPlatformBrowser(this.platformId)) {
        this.isBrowser = isPlatformBrowser(this.platformId);
        this.loader = true;
        this.http.get('user/verify/' + this.token).subscribe(
          data => {
            if (data.success) {
              this.isVerified = true;
              this.loader = false;
              // tslint:disable-next-line:max-line-length
              this.toasts.next({ text: 'Token Verified Successfully. You will be redirect to login page in 15 seconds.', caption: 'Success', type: 'success' });
              this.text = 'Token Verified Successfully. You will be redirect to login page in 30 seconds.';
              setTimeout(() => {
                this.router.navigate(['/login']);
              }, 17000);
            } else {
              this.token = 'invalid';
              this.text = data.error;
              this.loader = false;
            }
          }, error => {
            this.loader = false;
            this.toasts.next({ text: 'Invalid Token Detected.', caption: 'Fail', type: 'danger' });
          }
        );
      }
    }
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

}
