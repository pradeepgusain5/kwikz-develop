import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  styleUrls: ['./aboutus.component.scss']
})
export class AboutusComponent implements OnInit {

  constructor(public meta: Meta, public title: Title) { }

  ngOnInit() {
    this.title.setTitle('About Us: Buy & Sell, Online Classified NZ | Kwikz');
    this.meta.updateTag({
      // tslint:disable-next-line:max-line-length
      name: 'description', content: 'Kwikz is a product designed by the vision to provide a smooth car buying and selling to New Zealanders. Zero success fee & re-list until sold. Learn more'
    })
    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,about us'
    //   },
    // ]);
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

}
