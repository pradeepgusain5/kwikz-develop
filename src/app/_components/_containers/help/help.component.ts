import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { ToastNotifications } from 'ngx-toast-notifications';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { environment } from '../../../../environments/environment';
import { HttpServiceService } from '../../../_services';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {
  formData = {
    name: '', email: '', mobile: '', msg: '', image_url: '', topic: '', topicValidation: true, emailValidation: false
  };
  topics = [
    { name: 'SignUp/Login related issue' },
    { name: 'Listing issue' },
    { name: 'Payment related issue' },
    { name: 'Any Other Issue' }
  ];

  // Image Upload Starts
  files: File[];
  fileKeys: any = [];
  recentlySelected: any = [];
  // Image Upload Ends
  public loader: boolean;

  bucket: any = new S3({
    accessKeyId: environment.accessKeyId,
    secretAccessKey: environment.secretAccessKey,
    region: environment.region
  });

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    public _DomSanitizationService: DomSanitizer,
    private http: HttpServiceService,
    private toasts: ToastNotifications,
    private router: Router,
    public meta: Meta,
    public title: Title
  ) { }

  ngOnInit() {
    this.title.setTitle('Help Centre | Kwikz');

    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,help'
    //   }
    // ]);
    if (this.localStorage.getItem('token')) {
      this.loader = true;
      this.http.get('user/detail').subscribe(
        success => {
          this.loader = false;
          this.formData.name = success.user_detail.fname;
          this.formData.email = success.user_detail.email;
          this.formData.mobile = success.user_detail.mobile;
        }, error => {
          this.loader = false;
        }
      );
    }
    this.formData.topicValidation = false;
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  topicSelect(event) {
    this.formData.topicValidation = event.target.value === '1' ? true : false;
    if (!this.formData.topicValidation) {
      this.formData.topic = event.target.value;
    }
  }

  SubmitForm() {
    // email validation
    if (/(.+)@(.+){2,}\.(.+){2,}/.test(this.formData.email)) {
      // valid email
    } else {
      this.formData.emailValidation = true;
      return;
    }

    if (!this.formData.topic || (this.formData.topic === '1')) {
      this.formData.topicValidation = true;
    }

    // topic validation
    if (!this.formData.topicValidation) {
      if ((this.formData.name && this.formData.email && this.formData.msg)) {
        this.loader = true;
        if (this.files && this.files.length) {
          for (let file of this.files) {
            let params = {
              Bucket: 'bhaskar-kwikz',
              Key: 'user/help/' + Date.now() + '-' + Math.floor(Math.random() * (+9999999 - +100)) + +100 + '-' + file.name,
              Body: file,
              ACL: 'public-read',
              StorageClass: 'REDUCED_REDUNDANCY'
            };

            return this.bucket.upload(params, (err, data) => {
              if (err) {
                console.log('There was an error uploading your file: ', err);
                this.loader = false;
                return false;
              }
              this.formData.image_url = data['Location'];
              this.http.post('user/launchcontact', this.formData).subscribe(
                success => {
                  this.loader = false;
                  this.router.navigate(['/thanks']);
                }, error => {
                  this.loader = false;
                  this.router.navigate(['/thanks']);
                }
              );
            });
          }
        } else {
          this.http.post('user/launchcontact', this.formData).subscribe(
            success => {
              this.loader = false;
              this.router.navigate(['/thanks']);
            }, error => {
              this.loader = false;
              this.router.navigate(['/thanks']);
            }
          );
        }
      } else {
        this.toasts.next({ text: 'Field are missing', caption: 'Fail', type: 'danger' });
      }
    }
  }

  setImages(evt) {
    evt.preventDefault();
    this.recentlySelected = [];
    this.files = evt.target.files;
    let file = this.files[0];
    if (this.files && file) {
      // We iterate the array in the code
      for (let i = 0; i < this.files.length; i++) {
        let type = this.files[i].type;
        let reader = new FileReader();
        // reader.onload = e => this.recentlySelected.push(reader.result);
        reader.readAsDataURL(this.files[i]);
        this.recentlySelected.push(window.URL.createObjectURL(this.files[i]));
      }
    }
  }

  deleteSelectedImage() {
    this.files = [];
    this.fileKeys = [];
    this.recentlySelected = [];
  }
}
