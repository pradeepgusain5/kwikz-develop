import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-cookie',
  templateUrl: './cookie.component.html',
  styleUrls: ['./cookie.component.scss']
})
export class CookieComponent implements OnInit {

  constructor(public meta: Meta, public title: Title) { }

  ngOnInit() {
    this.title.setTitle('Cookie Policy | Kwikz');

    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,cookie'
    //   }
    // ]);
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

}
