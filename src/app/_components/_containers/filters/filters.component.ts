import { Component, OnInit } from '@angular/core';
import { Router, Params, ActivatedRoute } from '@angular/router';
import { ToastNotifications } from 'ngx-toast-notifications';
import { HttpServiceService } from '../../../_services';
import { environment } from '../../../../environments/environment';
import { isPlatformServer } from '@angular/common';
import { isPlatformBrowser } from '@angular/common';
import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import * as $ from 'jquery';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
// declare var jquery:any;
// declare var $ :any;

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {

  public loader: boolean;
  // yearArray = ["2010", '2011', '2012', '2013', '2014', '2015', '2016', '2017', '2018'];
  fuelTypeArray = ['Other', 'Petrol', 'Diesel', 'Hybrid', 'Electric'];
  maxPriceArray = ['1000$', '2000$', '3000$', '4000$', '5000$', '6000$', '7000$', '8000$', '9000$', '10000$'];
  // tslint:disable-next-line:max-line-length
  priceArray = ['1000', '2000', '3000', '4000', '5000', '7000', '7500', '10000', '12500', '15000', '20000', '25000', '30000', '35000', '40000', '50000', '60000', '70000', '80000', '90000', '100000', '150000', '200000'];
  kmsDriven = [
    '10000', '20000', '30000', '40000', '50000', '60000', '70000', '80000',
    '90000', '100000', '120000', '140000', '160000', '180000', '200000',
    '250000', '300000'];
  // tslint:disable-next-line:max-line-length
  locationArray = ['Northland', 'Auckland', 'Waikato', 'Bay of Plenty', 'Gisborne', 'Hawke\'s Bay', 'Taranaki', 'Whanganui', 'Manawatu', 'International', 'Wairarapa', 'Wellington', 'Nelson Bays', 'Marlborough', 'West Coast', 'Canterbury', 'Timaru - Oamaru', 'Otago', 'Southland', 'North Island', 'South Island', 'Other'];
  bodyTypeArray = ['Convertible', 'Coupe', 'Hatchback', 'Sedan', 'Station Wagon', 'RV/SUV', 'Ute', 'Van'];
  transmissionType = ['Automatic', 'Manual', 'Tiptronic', 'Others'];
  color = ['Silver', 'White', 'Black', 'Grey', 'Red', 'Blue', 'Green'];
  currentMakeIdValue = '';
  currentMakeValue = '';
  currentModelIdValue = '';
  currentModelValue = '';
  currentTitleValue = '';
  makeNameArray: any = [];
  modelNameArray: any = [];
  carYearArray: any = [];
  carYearToArray = [];
  maxPriceRangeArray = [];
  maxKmsRangeArray = [];
  fitlerFormMake: any;
  fitlerFormModel: any;
  filterFormLocation: any;
  fitlerFormYearFrom: any;
  fitlerFormYearTo: any;
  fitlerFormKmsFrom: any;
  fitlerFormKmsTo: any;
  fitlerFormPriceFrom: any;
  fitlerFormPriceTo: any;
  filterFormFuelType: any;
  filterFormBodyType: any;
  filterFormTransmissionType: any;
  filterFormColor: any;
  filterFormlistingId: any;
  mobilekeywordSearch: string;
  isBrowser: boolean;
  filterUploaded = false;
  makeControl = new FormControl();
  filteredMake: Observable<any[]>;
  modelControl = new FormControl();
  filteredModel: Observable<any[]>;
  filterData: any;
  public uiLoader: any = [];
  show: any = {
    make: false,
    model: false,
    year: false,
    price: false,
    fuel: false,
    kms: false,
    body: false,
    transmission: false,
    color: false,
    listing: false,
    location: false,
  };

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    @Inject(PLATFORM_ID) private platformId: any,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private http: HttpServiceService,
    private toasts: ToastNotifications,
    private ngxService: NgxUiLoaderService
  ) {
    this.filteredMake = this.makeControl.valueChanges.pipe(
      startWith(''),
      map(make => make ? this._filterMake(make) : this.makeNameArray.slice())
    );
    this.filteredModel = this.modelControl.valueChanges.pipe(
      startWith(''),
      map(model => model ? this._filterModel(model) : this.modelNameArray.slice())
    );
  }

  private _filterMake(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.makeNameArray.filter(make => make.make.toLowerCase().indexOf(filterValue) === 0);
  }

  private _filterModel(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.modelNameArray.filter(model => model.model.toLowerCase().indexOf(filterValue) === 0);
  }

  makeFocus() {
    if (!this.currentMakeValue) {
      this.currentMakeValue = '';
    }
  }

  modelFocus() {
    if (!this.currentModelValue) {
      this.currentModelValue = '';
    }
  }

  ngOnInit() {
    if (isPlatformBrowser(this.platformId)) {
      this.isBrowser = isPlatformBrowser(this.platformId);
      this.http.postNode('carmakemodel/select=make', {}).subscribe(
        data => {
          this.makeNameArray = data.result;
          this.uiLoader['make-loader'] = false;
          this.getFilterDataFromRoute();
        }, error => {
          this.toasts.next({ text: 'Unable to fetch car make options', caption: 'Fail', type: 'danger' });
        }
      );
    }
    this.uiLoader['make-loader'] = true;
    this.uiLoader['model-loader'] = true;
    this.filterUploaded = false;
    this.activatedRoute.queryParams.subscribe(
      (params: Params) => {
        this.filterData = params;
      });
    // this.http.postNode('carmakemodel/select=make', {}).subscribe(
    //   data => {
    //     this.makeNameArray = data.result;
    //     this.uiLoader['make-loader'] = false;
    //     this.getFilterDataFromRoute();
    //   }, error => {
    //     this.toasts.next({ text: 'Unable to fetch car make options', caption: 'Fail', type: 'danger' });
    //   }
    // );
  }

  getFilterDataFromRoute() {
    if (this.filterData) {
      this.currentTitleValue = this.filterData.keyword;
      this.currentMakeValue = this.filterData.make;
      this.currentModelValue = this.filterData.model;
      this.fitlerFormYearFrom = this.filterData.year_manufacture_from;
      this.fitlerFormYearTo = this.filterData.year_manufacture_to;
      this.fitlerFormKmsFrom = this.filterData.kilometers_used_from;
      this.fitlerFormKmsTo = this.filterData.kilometers_used_to;
      this.filterFormFuelType = this.filterData.fuel_type;
      this.filterFormBodyType = this.filterData.body_style;
      this.filterFormTransmissionType = this.filterData.transmission;
      this.filterFormColor = this.filterData.color;
      this.filterFormlistingId = this.filterData.listing_id;
      this.fitlerFormPriceFrom = this.filterData.price_from;
      this.fitlerFormPriceTo = this.filterData.price_to;
      this.filterFormLocation = this.filterData.location;
    }
    if (this.currentMakeValue) {
      // tslint:disable-next-line:triple-equals
      let make = this.makeNameArray.find(e => e.make == this.currentMakeValue);
      this.makeChanged(make);
    }
  }

  formatLabel(value: number | null) {
    if (!value) {
      return 0;
    }

    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }

    return value;
  }

  makeChanged(make) {
    this.fitlerFormMake = make.make;
    this.filterUploaded = false;
    this.currentMakeValue = make.make;
    this.currentMakeIdValue = make.make_id;
    this.getFilterPost();   // filtering result & fetching model options
    this.uiLoader['model-loader'] = true;
    this.http.postNode(`carmakemodel/select=model&make_id=${make.make_id}`, {}).subscribe(
      data => {
        this.uiLoader['model-loader'] = false;
        this.filterUploaded = true;
        this.modelNameArray = data.result;
      }, error => {
        this.filterUploaded = true;
        this.toasts.next({ text: 'Unable to fetch car model options', caption: 'Fail', type: 'danger' });
      }
    );
  }

  filterValues(search: string) {
    return this.modelNameArray.filter(value =>
      value.toLowerCase().indexOf(search.toLowerCase()) === 0);
  }

  modelChanged(model) {
    this.filterUploaded = false;
    this.currentModelValue = model.model;
    this.getFilterPost();   // filtering result & fetching year options
    this.http.postNode(`carmakemodel/select=year&make_id=${this.currentMakeIdValue}&model_id=${model.model_id}`, {}).subscribe(
      data => {
        this.filterUploaded = true;
        this.carYearArray = data.result;
      }, error => {
        this.filterUploaded = true;
        this.toasts.next({ text: 'Unable to fetch year options', caption: 'Fail', type: 'danger' });
      }
    );
  }

  getYearFrom(yearFromVal) {
    this.fitlerFormYearFrom = yearFromVal;
    let tempArr = [];
    for (let i = 0; i < this.carYearArray.length; i++) {
      tempArr[i] = this.carYearArray[i].year;
    }
    let removeFrom = tempArr.indexOf(yearFromVal.year);
    let to = tempArr.slice(removeFrom + 1, tempArr.length);
    this.carYearToArray = to;
  }

  setManufactureYearTo(yearFrom, YearTo) {
    this.fitlerFormYearFrom = yearFrom;
    // this.getFilterPost();
  }

  getMinPrice(minPriceVal) {
    let minPriceArray = [];
    for (let i = 0; i < this.priceArray.length; i++) {
      minPriceArray[i] = this.priceArray[i];
    }
    let removeFrom = minPriceArray.indexOf(minPriceVal);
    let maxPrice = minPriceArray.slice(removeFrom + 1, minPriceArray.length);
    this.maxPriceRangeArray = maxPrice;
    // this.getFilterPost();
  }

  setMaxPrice(minPrice, maxPrice) {
    this.fitlerFormPriceFrom = minPrice;
    this.fitlerFormPriceTo = maxPrice;
    // this.getFilterPost();
  }

  getMinKmsDriven(minKmsDrivenVal) {
    let minKmsArray = [];
    for (let i = 0; i < this.kmsDriven.length; i++) {
      minKmsArray[i] = this.kmsDriven[i];
    }
    let removeFrom = minKmsArray.indexOf(minKmsDrivenVal);
    let maxKms = minKmsArray.slice(removeFrom + 1, minKmsArray.length);
    this.maxKmsRangeArray = maxKms;
    // this.getFilterPost();
  }

  setKmsUsed(from, to) {
    this.fitlerFormKmsFrom = from;
    this.fitlerFormKmsTo = to;
    // this.getFilterPost();
  }

  getFuelType(fuelTypeVal) {
    this.filterFormFuelType = fuelTypeVal;
    this.getFilterPost();
  }

  getBodyType(bodyTypeVal) {
    this.filterFormBodyType = bodyTypeVal;
    this.getFilterPost();
  }

  getTransmissionType(transmissionTypeVal) {
    this.filterFormTransmissionType = transmissionTypeVal;
    this.getFilterPost();
  }

  getColor(color) {
    this.filterFormColor = color;
    this.getFilterPost();

  }

  getListingId(listingId) {
    if (listingId !== '') {
      this.filterFormlistingId = listingId;
      this.getFilterPost();
    }
  }

  getLocation(location) {
    this.filterFormLocation = location;
    this.getFilterPost();
  }

  listFormatter = (data: any) => `<span>${data.make}</span>`;
  modelFormatter = (data: any) => `<span>${data.model}</span>`;
  yearFormatter = (data: any) => `<span>${data.year}</span>`;

  keywordSearch(event, keyword) {
    this.currentTitleValue = keyword;
    this.getFilterPost();
  }

  getFilterPost() {
    let filterData = {
      keyword: this.currentTitleValue ? this.currentTitleValue : '',
      make: this.currentMakeValue ? this.currentMakeValue : '',
      model: this.currentModelValue ? this.currentModelValue : '',
      year_manufacture_from: this.fitlerFormYearFrom ? this.fitlerFormYearFrom : '',
      year_manufacture_to: this.fitlerFormYearTo ? this.fitlerFormYearTo : '',
      kilometers_used_from: this.fitlerFormKmsFrom ? this.fitlerFormKmsFrom : '',
      kilometers_used_to: this.fitlerFormKmsTo ? this.fitlerFormKmsTo : '',
      fuel_type: this.filterFormFuelType ? this.filterFormFuelType : '',
      body_style: this.filterFormBodyType ? this.filterFormBodyType : '',
      transmission: this.filterFormTransmissionType ? this.filterFormTransmissionType : '',
      color: this.filterFormColor ? this.filterFormColor : '',
      listing_id: this.filterFormlistingId ? this.filterFormlistingId : '',
      price_from: this.fitlerFormPriceFrom ? this.fitlerFormPriceFrom : '',
      price_to: this.fitlerFormPriceTo ? this.fitlerFormPriceTo : '',
      location: this.filterFormLocation ? this.filterFormLocation : ''
    };

    // console.log('filterData', filterData);
    this.localStorage.setItem('filters', JSON.stringify(filterData));
    this.router.navigate([environment.filterUrl], { queryParams: filterData });
  }

  clearAllFilter() {
    this.router.navigate(['/']).then(() => this.router.navigate(['/used-cars-search']));
  }

  mobileFilterMore() {
    if (isPlatformBrowser(this.platformId)) {
      $('.filter_box_mob').slideToggle();
    }
  }
  closeMobilefilter() {
    if (isPlatformBrowser(this.platformId)) {
      $('.filter_box_mob').slideUp();
    }
  }

}
