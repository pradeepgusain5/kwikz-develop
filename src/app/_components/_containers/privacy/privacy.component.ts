import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.scss']
})
export class PrivacyComponent implements OnInit {

  constructor(public meta: Meta, public title: Title) { }

  ngOnInit() {
    this.title.setTitle('Privacy Policy | Kwikz');

    // this.meta.addTags([
    //   {
    //     name: 'author', content: 'Kwikz'
    //   },
    //   {
    //     name: 'keywords', content: 'kwikz,privacy policy'
    //   }
    // ]);
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

}
