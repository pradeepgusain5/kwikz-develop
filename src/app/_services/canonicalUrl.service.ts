import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable({
    providedIn: 'root'
})

export class CanonicalService {
    constructor(@Inject(DOCUMENT) private dom) { }

    createCanonicalURL(url?: string) {
        let canURL = url ? `https://www.kwikz.nz${url}` : this.dom.URL;
        // console.log("::::::::::::", canURL)
        // let allLinks = document.getElementsByTagName('link');
        // console.log(allLinks[0])
        // allLinks[0].setAttribute('href', canURL)
        // let allLinksUp = document.getElementsByTagName('link');
        // console.log(allLinksUp[0])
        let link: HTMLLinkElement = this.dom.createElement('link');
        link.setAttribute('rel', 'canonical');
        this.dom.head.appendChild(link);
        link.setAttribute('href', canURL);
    }
}
