import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable ,  Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { HttpServiceService } from './http-service.service';

@Injectable()
export class AuthenticationService {
  public token: string;
  public headers: HttpHeaders;
  public readonly apiUrl = environment.apiUrl;
  public readonly nodeApiUrl = environment.nodeApiUrl;
  public readonly baseUrl = environment.baseUrl;
  public IsUserLoggedIn: Subject<boolean> = new Subject<boolean>();

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    public http: HttpClient,
    public httpService: HttpServiceService
  ) {
    if (this.localStorage.getItem('user')) {
      let currentUser = JSON.parse(this.localStorage.getItem('user'));
      this.token = currentUser && currentUser.token;
    }
  }

  isLoggedIn() {
    if (this.localStorage.getItem('user')) {
      return true;
    }
    return false;
  }

  login(data: any): Observable<any> {
    return this.http.post(this.apiUrl + 'auth/login', data)
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  register(data): Observable<any> {
    return this.http.post(this.apiUrl + 'auth/signup', data)
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  dealerRegister(data): Observable<any> {
    return this.http.post(this.apiUrl + 'dealer-businesses', data)
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  dealerUpdate(id, data): Observable<any> {
    let url = 'dealer-businesses/' + id;
    return this.httpService.patch(url, data)
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  dealerDetail(id): Observable<any> {
    return this.http.get(this.apiUrl + 'dealer-businesses/' + id + '?with=owner')
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    this.localStorage.removeItem('user');
  }

  sendPasswordResetEmail(email: string): Observable<any> {
    // tslint:disable-next-line:object-literal-shorthand
    return this.http.post(this.apiUrl + 'user/passwordreset', { email: email })
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  // mail to dealer
  sendDealerRegistrationMail(data): Observable<any> {
    return this.httpService.postNode('email/dealerRegister', data)
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  // mail to admin
  sendDealerRegistrationAdminMail(data): Observable<any> {
    return this.httpService.postNode('email/dealerRegisterNotification', data)
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  verifyTokenForResetPassword(token: string): Observable<any> {
    return this.http.get(this.apiUrl + 'user/passwordchange/' + token)
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  resetUserPassword(userDetail: any): Observable<any> {
    return this.http.post(this.apiUrl + 'user/passwordchange/set', userDetail)
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  resetPassword(newPassword: string, confirmedPassword: string, token: string): Observable<any> {
    // tslint:disable-next-line:object-literal-shorthand
    return this.http.post(this.apiUrl + '/auth/reset', { password: newPassword, password_confirmation: confirmedPassword, token: token })
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }

  getProfile(): Observable<any> {
    return this.http.get(this.apiUrl + 'user/detail')
      .pipe(
        map((response: Response) => {
          return response;
        })
      );
  }
}
