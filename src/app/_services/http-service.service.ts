import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as Rx from 'rxjs';
// tslint:disable-next-line:import-blacklist
import 'rxjs/Rx';
import { environment } from '../../environments/environment';

@Injectable()
export class HttpServiceService {
  loggedIn = new Rx.BehaviorSubject(this.localStorage.getItem('token') ? true : false);
  lastUrl = new Rx.BehaviorSubject('');

  // tslint:disable-next-line: deprecation
  private headers = new Headers({
    'Content-Type': 'application/json',
    'Authorization': 'Bearer ' + this.localStorage.getItem('token')
  });

  // tslint:disable-next-line: deprecation
  private options = new RequestOptions({ headers: this.headers });

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    // tslint:disable-next-line: deprecation
    private http: Http
  ) { }

  private configure() {
    let token = this.localStorage.getItem('token');
    // tslint:disable-next-line: deprecation
    let headers = new Headers();
    // tslint:disable-next-line: deprecation
    headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });
    // tslint:disable-next-line: deprecation
    this.options = new RequestOptions({ headers });
  }

  public post(url: string, data: {}): Observable<any> {
    this.configure();
    // tslint:disable-next-line: rxjs-pipeable-operators-only
    return this.http.post(environment.apiUrl + url, data, this.options).map(res => {
      return res.json();
    });
  }

  public patch(url: string, data: {}): Observable<any> {
    this.configure();
    // tslint:disable-next-line: rxjs-pipeable-operators-only
    return this.http.patch(environment.apiUrl + url, data, this.options).map(res => {
      return res.json();
    });
  }

  public postNode(url: string, data: {}): Observable<any> {
    // tslint:disable-next-line: rxjs-pipeable-operators-only
    return this.http.post(environment.nodeApiUrl + url, data).map(res => {
      return res.json();
    });
  }

  public get(url: string): Observable<any> {
    this.configure();
    // tslint:disable-next-line: rxjs-pipeable-operators-only
    return this.http.get(environment.apiUrl + url, this.options).map(res => {
      return res.json();
    });
  }

  public getNode(url: string): Observable<any> {
    // tslint:disable-next-line: rxjs-pipeable-operators-only
    return this.http.get(environment.nodeApiUrl + url, this.options).map(res => {
      return res.json();
    });
  }

  public getBlog(url: string): Observable<any> {
    // tslint:disable-next-line: rxjs-pipeable-operators-only
    return this.http.get(environment.apiBlog + url).map(res => {
      return res.json();
    });
  }

  isLoggedFn(): Observable<any> {
    return this.loggedIn;
  }

}
