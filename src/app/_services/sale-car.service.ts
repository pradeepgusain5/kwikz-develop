import { Injectable } from '@angular/core';
import { HttpServiceService } from './http-service.service';

@Injectable()
export class SaleCarService {

  constructor(public http: HttpServiceService) { }

  getCarDetails(plateNumber) {
    return this.http.postNode('carjam/' + plateNumber, {});
  }

  // getCarDetails(plate_Number) {
  //     return this.http.post('post/carjam', {plateNumber: plate_Number});
  // }

  getCar(carId) {
    return this.http.post('post/show', { post_id: carId });
  }

  storeCar(newCar) {
    return this.http.post('post/store', newCar);
  }

  checkDuplicatePost(plateNumber) {
    return this.http.post('post/duplicatecheck', plateNumber);
  }

  storeCarInOneStep(newCar) {
    return this.http.post('post/createonestep', newCar);
  }

  updateCar(newCar) {
    return this.http.post('post/update1', newCar);
  }

  // updateCarDetails() {

  // }

  getMakes() {
    return this.http.postNode('carmakemodel/select=make', {});
  }

  getModels(makeId) {
    return this.http.postNode('carmakemodel/select=model&make_id=' + makeId, {});
  }

  getYears(makeId, modelId) {
    return this.http.postNode('carmakemodel/select=year&make_id=' + makeId + '&model_id=' + modelId, {});
  }

  getCylinders() {

  }

  getFuelTypes() {

  }

  getBodyTypes() {

  }

  getTransmissions() {

  }

  uploadImages(images, postId) {
    // tslint:disable-next-line:object-literal-shorthand
    return this.http.post('image/upload', { post_id: postId, images: images });
  }

  deleteImage(path) {
    return this.http.post('image/delete', { image_path: path });
  }

  timeConverter(UNIXTimestamp, type) {
    let a = new Date(UNIXTimestamp * 1000);
    let todayDate = new Date();
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let year = a.getFullYear();
    let month = months[a.getMonth()];
    let date = a.getDate();
    let hour = a.getHours();
    let min = a.getMinutes();
    let sec = a.getSeconds();
    let time = month + ' ' + year;
    if (a < todayDate) {
      if (type === 0) {
        time = 'No Rego';
      } else {
        time = 'No WOF';
      }
    }
    return time;
  }

  getMultyMonths(fromYear, toYear, addDefault) {
    let date = new Date();
    let currentMonth = date.getMonth();
    let times: any = [];
    times = addDefault;
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    for (let year = fromYear; year <= toYear; ++year) {
      let startFrom = 0;
      let totalMonth = months.length
      if (year === fromYear) {
        startFrom = currentMonth;
      }
      if (year === toYear) {
        totalMonth = currentMonth
      }
      for (let monthKey = startFrom; monthKey < totalMonth; ++monthKey) {
        times.push(months[monthKey] + ' ' + year);
      }
    }
    return times;
  }

}
