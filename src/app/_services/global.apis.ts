'use strict';

export const baseAPIPath = 'http://api.kwikz.nz/api';
export const loginAPIPath = 'http://api.kwikz.nz/api/auth/login';
export const registerAPIPath = 'http://api.kwikz.nz/api/auth/signup';
export const createAPIPath = 'http://api.kwikz.nz/api/post/store';
export const updateAPIPath = 'http://api.kwikz.nz/api/post/update1';
export const showPostAPIPath = 'http://api.kwikz.nz/api/post/public';
export const KeywordFilterAPIPath = 'http://api.kwikz.nz/api/post/filter';
export const singlePostAPIPath = 'http://api.kwikz.nz/api/post/show';
export const imageUpload = 'http://api.kwikz.nz/api/image/multiupload';
export const carjamApi = 'http://node.kwikz.nz:3000/api/carjam';
export const postAPIPath = '';
export const uploadsBucketPath = 'https://s3-ap-southeast-2.amazonaws.com/kwikzupload/';
