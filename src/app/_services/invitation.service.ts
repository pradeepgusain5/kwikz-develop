import { Injectable } from '@angular/core';
import { HttpServiceService } from './http-service.service';
import { Observable } from 'rxjs';

@Injectable()
export class InvitationService {

    constructor(public http: HttpServiceService) { }

    getTopInvitees(): Observable<any> {
        return this.http.get('user/topinvitees');
    }
}
