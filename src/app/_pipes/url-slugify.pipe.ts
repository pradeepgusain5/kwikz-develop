import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../environments/environment';

@Pipe({
  name: 'slugify'
})
export class UrlSlugify implements PipeTransform {
  carPostUrl: string = environment.carPostUrl;

  transform(input: any): any {
    if (input.id) {
      return '/blog/' + input.key.toString().toLowerCase().replace(/\s+/g, '-') //+ '?id=' + input.id;
    } if (input.type == 'category') {
      return '/blogs/' + input.key.toString().toLowerCase().replace(/\s+/g, '-');
    }
    else {
      return '/blogs/' //+ input.key.toString().toLowerCase().replace(/\s+/g, '-');
    }
  }

}
