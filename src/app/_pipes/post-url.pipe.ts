import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../environments/environment';

@Pipe({
  name: 'posturl'
})
export class PostUrl implements PipeTransform {
  carPostUrl: string = environment.carPostUrl;

  transform(input: any): any {
    input["region"] = input["region"] ? input["region"].toString().toLowerCase().replace(/\s+/g, '-') : '-';
    input["make"] = input["make"] ? input["make"].toString().toLowerCase().replace(/\s+/g, '-') : '-';
    input["model"] = input["model"] ? input["model"].toString().toLowerCase().replace(/\s+/g, '-') : '-';
    input["year_manufacture"] = input["year_manufacture"] ? input["year_manufacture"].toString().toLowerCase().replace(/\s+/g, '-') : '-';
    input["id"] = input["id"] ? input["id"].toString().toLowerCase().replace(/\s+/g, '-') : '-';
    input["city"] = input["city"] ? input["city"].toString().toLowerCase().replace(/\s+/g, '-') : '-';
    if (input.region === input.city) {
      // tslint:disable-next-line:max-line-length
      return this.carPostUrl + '/' + input.region.toLowerCase() + '/' + input.make.toLowerCase() + '/' + input.model.toLowerCase() + '/' + input.year_manufacture + '/' + input.id;
    } else {
      // tslint:disable-next-line:max-line-length
      return this.carPostUrl + '/' + input.region.toLowerCase() + '-' + input.city.toLowerCase() + '/' + input.make.toLowerCase() + '/' + input.model.toLowerCase() + '/' + input.year_manufacture + '/' + input.id;
    }
  }

}
