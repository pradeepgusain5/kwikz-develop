import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule, TitleCasePipe } from '@angular/common';
// all angular imports
import { BrowserModule } from '@angular/platform-browser';
import { MatButtonModule, MatChipsModule, MatIconModule, MatInputModule, MatCheckboxModule } from '@angular/material';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LayoutModule } from '@angular/cdk/layout';
import { ToastNotificationClientModule, ToastNotificationCoreModule } from 'ngx-toast-notifications';
import { Ng4GeoautocompleteModule } from 'ng4-geoautocomplete';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
import { SlideshowModule } from 'ng-simple-slideshow';
import { StripeCheckoutModule } from 'ng-stripe-checkout';
import { NguCarouselModule } from '@ngu/carousel';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ModalModule } from 'ngx-bootstrap';

// all app modules
import { MaterialModule } from './_modules/index';
import { routing } from './app-routing.module';

// all guards
import { AuthGuard } from './_guards/auth.guard';

// all intercepters
import { JwtInterceptor } from './_helpers';

// all services
import { AuthenticationService, HttpServiceService, SaleCarService, InvitationService } from './_services/index';

// all components
import { AppComponent } from './_components/app.component';
import { LoginComponent, RegisterComponent, ForgotPasswordComponent, ResetPasswordComponent } from './_components/auth/index';
import { NavigationComponent } from './_components/navigation/navigation.component';
import { HomeComponent } from './_components/home/home.component';
import { FooterComponent } from './_components/footer/footer.component';
import { SellUsedCarComponent } from './_components/sell-used-car/sell-used-car.component';
import { FiltersComponent } from './_components/_containers/filters/filters.component';
import { AboutusComponent } from './_components/_containers/aboutus/aboutus.component';
import { TermsComponent } from './_components/_containers/terms/terms.component';
import { CookieComponent } from './_components/_containers/cookie/cookie.component';
import { PrivacyComponent } from './_components/_containers/privacy/privacy.component';
import { ThankyouComponent } from './_components/_containers/thankyou/thankyou.component';
import { HelpComponent } from './_components/_containers/help/help.component';
import { MyaccountComponent } from './_components/myaccount/myaccount.component';
import { SellCarComponent } from './_components/sell-used-car/sell-car/sell-car.component';
import { UsedCarComponent } from './_components/sell-used-car/used-car/used-car.component';
import { UsedCarSearchComponent } from './_components/sell-used-car/used-car-search/used-car-search.component';
import { RelatedadsComponent } from './_components/_containers/relatedads/relatedads.component';

import { BlogComponent, BlogDetailComponent, RecentPosts } from './_components/blog/index';
import { PostExpireComponent } from './_components/redirects/postExpire/postExpire.component';
import { InputChip } from './_components/_containers/reusable_components/inputchips/inputChip.component';
import { FilterPipeModule } from 'ngx-filter-pipe';

// Directives
import { PlaceLookupDirective } from './_directives/place-lookup.directive';

// Pipes
import { MomentPipe } from './_pipes/moment-pipe.pipe';
import { UrlSlugify } from './_pipes/url-slugify.pipe';
// import { PostUrl } from './_pipes/post-url.pipe';
import { StripeComponent } from './_components/stripe/stripe.component';
import { SingleItemComponent } from './_components/_containers/carousal/single-item/single-item.component';
import { MultiItemComponent } from './_components/_containers/carousal/multi-item/multi-item.component';
import { DealerModule } from './_modules/dealer/dealer.module';
import { SellMultiCarComponent } from './_components/sell-multi-car/sell-multi-car.component';
import { ConfirmationComponent } from './_components/sell-multi-car/confirmation/confirmation.component';
import { SharedModule } from './_modules/shared.module';
import { SelectAutocompleteModule } from 'mat-select-autocomplete';
import { InviteLandingPageComponent } from './_components/invite-landing-page/invite-landing-page.component';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { FilterLoaderComponent } from './_components/_containers/filter-loader/filter-loader.component';
import { NoPageFoundComponent } from './_components/_containers/no-page-found/no-page-found.component';
import { MarkdownModule } from "ngx-markdown";
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    NavigationComponent,
    HomeComponent,
    FooterComponent,
    SellUsedCarComponent,
    FiltersComponent,
    AboutusComponent,
    TermsComponent,
    CookieComponent,
    PrivacyComponent,
    ThankyouComponent,
    HelpComponent,
    MyaccountComponent,
    SellCarComponent,
    UsedCarComponent,
    UsedCarSearchComponent,
    PlaceLookupDirective,
    MomentPipe,
    UrlSlugify,
    // PostUrl,
    RelatedadsComponent,
    StripeComponent,
    SingleItemComponent,
    MultiItemComponent,
    SellMultiCarComponent,
    ConfirmationComponent,
    BlogComponent,
    BlogDetailComponent,
    RecentPosts,
    PostExpireComponent,
    InputChip,
    InviteLandingPageComponent,
    FilterLoaderComponent,
    NoPageFoundComponent
  ],
  imports: [
    MarkdownModule.forRoot(),
    CommonModule,
    NgtUniversalModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    HttpClientModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    LayoutModule,
    MaterialModule,
    ToastNotificationCoreModule.forRoot({ lifetime: 4000 }),
    ToastNotificationClientModule,
    Ng4GeoautocompleteModule.forRoot(),
    NguiAutoCompleteModule.forRoot(),
    SlideshowModule,
    StripeCheckoutModule,
    NguCarouselModule,
    CarouselModule,
    DealerModule,
    BrowserModule,
    MatButtonModule,
    MatChipsModule,
    MatIconModule,
    MatInputModule,
    MatCheckboxModule,
    SharedModule,
    SelectAutocompleteModule,
    ModalModule,
    FilterPipeModule,
    NgxUiLoaderModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    AuthGuard,
    AuthenticationService,
    HttpServiceService,
    SaleCarService,
    TitleCasePipe,
    InvitationService
  ],
  exports: [
    // PostUrl
  ]
})
export class AppModule { }
