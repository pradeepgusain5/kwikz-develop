import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent, LoginComponent, ForgotPasswordComponent, ResetPasswordComponent } from './_components/auth/index';
import { HomeComponent } from './_components/home/home.component';
import { AuthGuard } from './_guards/auth.guard';
import { SellUsedCarComponent } from './_components/sell-used-car/sell-used-car.component';
import { AboutusComponent } from './_components/_containers/aboutus/aboutus.component';
import { TermsComponent } from './_components/_containers/terms/terms.component';
import { CookieComponent } from './_components/_containers/cookie/cookie.component';
import { PrivacyComponent } from './_components/_containers/privacy/privacy.component';
import { ThankyouComponent } from './_components/_containers/thankyou/thankyou.component';
import { HelpComponent } from './_components/_containers/help/help.component';
import { MyaccountComponent } from './_components/myaccount/myaccount.component';
import { SellCarComponent } from './_components/sell-used-car/sell-car/sell-car.component';
import { UsedCarComponent } from './_components/sell-used-car/used-car/used-car.component';
import { UsedCarSearchComponent } from './_components/sell-used-car/used-car-search/used-car-search.component';
import { RegistrationComponent } from './_modules/dealer/_components/auth/registration/registration.component';
import { SellMultiCarComponent } from './_components/sell-multi-car/sell-multi-car.component';
import { BlogComponent, BlogDetailComponent } from './_components/blog/index';
import { DealerEditComponent } from './_modules/dealer/_components/dealer-edit/dealer-edit.component';
import { PostExpireComponent } from './_components/redirects/postExpire/postExpire.component';
import { ProfileComponent } from './_modules/dealer/_components/profile/profile.component';
import { DealerThankYouComponent } from './_modules/dealer/_components/dealer-thank-you/dealer-thank-you.component';
import { InviteLandingPageComponent } from './_components/invite-landing-page/invite-landing-page.component';
import { NoPageFoundComponent } from './_components/_containers/no-page-found/no-page-found.component';
import { DealerLandingPageComponent } from './_modules/dealer/_components/dealer-landing-page/dealer-landing-page.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: RegisterComponent },
  { path: 'forgot', component: ForgotPasswordComponent },
  { path: 'reset-password/:token', component: ResetPasswordComponent },
  { path: 'home', component: HomeComponent/*, canActivate: [AuthGuard] */ },
  { path: 'sell-used-car', component: SellUsedCarComponent },
  { path: 'about', component: AboutusComponent },
  { path: 'terms', component: TermsComponent },
  { path: 'cookie', component: CookieComponent },
  { path: 'privacy', component: PrivacyComponent },
  { path: 'thanks', component: ThankyouComponent },
  { path: 'verify/:token', component: ThankyouComponent },
  { path: 'help-center', component: HelpComponent/*, canActivate: [AuthGuard] */ },
  { path: 'myaccount', component: MyaccountComponent/*, canActivate: [AuthGuard] */ },
  { path: 'sell-car/:id', component: SellCarComponent },
  { path: 'sell-multi-car', component: SellMultiCarComponent },
  // { path: 'used-car/:region/:city/:make/:model/:year/:id', component: UsedCarComponent },
  { path: 'used-car/:regioncity/:make/:model/:year/:id', component: UsedCarComponent },
  { path: 'used-cars-search', component: UsedCarSearchComponent },
  { path: 'business-account', component: DealerLandingPageComponent },
  { path: 'dealer/register', component: RegistrationComponent, canActivate: [AuthGuard] },
  { path: 'thank-you', component: DealerThankYouComponent, canActivate: [AuthGuard] },
  { path: 'dealer/edit/:id', component: RegistrationComponent, canActivate: [AuthGuard] },
  { path: 'dealer/:id', component: DealerEditComponent, canActivate: [AuthGuard] },
  { path: 'car-dealer/:id/:name/:dealerId', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'blogs', component: BlogComponent },
  { path: 'blogs/:category', component: BlogComponent },
  { path: 'blog/:title', component: BlogDetailComponent },
  { path: 'post/redirect/:id', component: PostExpireComponent },
  { path: 'invite', component: InviteLandingPageComponent, canActivate: [AuthGuard] },
  { path: '', component: HomeComponent },
  { path: '**', component: NoPageFoundComponent }
];

export const routing = RouterModule.forRoot(appRoutes);
