import { Component } from '@angular/core';

@Component({
  selector: 'loader',
  styles: [`
  a{
    text-decoration: none;
  }
  .loader_wrap{
    height: 100vh;
    background-color:rgba(218, 218, 218, 0.7);
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
  }
  #loader-6{
    position: absolute;
    top: 50%;
    left: 0;
    right: 0;
    margin:0px auto;
    transform: translateY(-50%);
    width: 100%;
    text-align:center;
  }
  #loader-6 span{
    display: inline-block;
    width: 5px;
    height: 20px;
        background-color: #3498db;
  }
  #loader-6 span:nth-child(1){
    animation: grow 1s ease-in-out infinite;
  }
  #loader-6 span:nth-child(2){
    animation: grow 1s ease-in-out 0.15s infinite;
  }
  #loader-6 span:nth-child(3){
    animation: grow 1s ease-in-out 0.30s infinite;
  }
  #loader-6 span:nth-child(4){
    animation: grow 1s ease-in-out 0.45s infinite;
  }
  @keyframes grow{
    0%, 100%{
      -webkit-transform: scaleY(1);
      -ms-transform: scaleY(1);
      -o-transform: scaleY(1);
      transform: scaleY(1);
    }
    50%{
      -webkit-transform: scaleY(1.8);
      -ms-transform: scaleY(1.8);
      -o-transform: scaleY(1.8);
      transform: scaleY(1.8);
    }
  }
  `],
  template: `<div class= "loader_wrap">
    <div id="loader-6">
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>`
})

export class LoaderComponent {

}
