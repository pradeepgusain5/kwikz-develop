import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './_components/auth/registration/registration.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastNotificationClientModule, ToastNotificationCoreModule } from 'ngx-toast-notifications';
import { DealerEditComponent } from './_components/dealer-edit/dealer-edit.component';
import { RouterModule } from '@angular/router';
import { Ng4GeoautocompleteModule } from 'ng4-geoautocomplete';
import { ProfileComponent } from './_components/profile/profile.component';
import { SharedModule } from '../shared.module';
import { DealerThankYouComponent } from './_components/dealer-thank-you/dealer-thank-you.component';
import { NgxUiLoaderModule } from 'ngx-ui-loader';
import { DealerLandingPageComponent } from './_components/dealer-landing-page/dealer-landing-page.component';

@NgModule({
  declarations: [
    RegistrationComponent,
    DealerEditComponent,
    ProfileComponent,
    DealerThankYouComponent,
    DealerLandingPageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ToastNotificationCoreModule.forRoot({ lifetime: 4000 }),
    ToastNotificationClientModule,
    RouterModule,
    Ng4GeoautocompleteModule.forRoot(),
    SharedModule,
    NgxUiLoaderModule
  ]
})
export class DealerModule { }
