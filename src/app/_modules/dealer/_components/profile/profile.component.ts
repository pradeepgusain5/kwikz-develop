import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpServiceService, AuthenticationService } from '../../../../_services';
import { environment } from '../../../../../environments/environment';
import { ToastNotifications } from 'ngx-toast-notifications';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  userId: any;
  dealerId: any;
  dealer: any;
  user: any;
  post: any;
  carNoImage: string = environment.CarNoImage;
  public loader = true;

  constructor(
    private http: HttpServiceService,
    private auth: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private toasts: ToastNotifications
  ) {
    this.route.params.subscribe(
      params => {
        this.userId = params.id;
        this.dealerId = params.dealerId;
      }
    );
  }

  ngOnInit() {
    this.loader = true;
    this.getDetail();
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  getDetail() {
    this.auth.dealerDetail(this.dealerId).subscribe(
      data => {
        this.dealer = data.dealer_business;
        this.user = data.dealer_business.owner;
        this.loader = false;
        if (this.user.is_dealer && this.user.dealer_business && this.user.dealer_business.is_verified) {
          this.loader = true;
          this.getPost();
        } else {
          this.router.navigate(['/']);
        }
      }, error => {
        this.toasts.next({ text: 'Something went wrong', caption: 'Fail', type: 'danger' });
      }
    );
  }

  getPost() {
    let dealer = {
      user_id: +this.userId
    };
    this.http.post('dealer/listing', dealer).subscribe(
      data => {
        this.loader = false;
        this.post = data.data;
      }, error => {
        this.loader = false;
        this.toasts.next({ text: 'Something went wrong', caption: 'Fail', type: 'danger' });
      }
    );
  }

}
