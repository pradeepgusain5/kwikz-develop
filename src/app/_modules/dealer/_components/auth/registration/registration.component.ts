import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ToastNotifications } from 'ngx-toast-notifications';
import { AuthenticationService } from '../../../../../_services';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from '../../../../../../environments/environment';
import * as S3 from 'aws-sdk/clients/s3';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  bucket: any = new S3({
    accessKeyId: environment.accessKeyId,
    secretAccessKey: environment.secretAccessKey,
    region: environment.region
  });
  registrationForm: FormGroup;
  submitted = false;
  licence: any;
  traderLicence: any;
  traderLicenceImg: any;
  licenceImg: any;
  // tslint:disable-next-line:variable-name
  dealer_id: any;
  // tslint:disable-next-line:variable-name
  company_logo: any;
  // tslint:disable-next-line:variable-name
  company_logo_img: any;
  public autoCompleteSettings: any = {
    showRecentSearch: false,
    geoCountryRestriction: ['nz'],
    searchIconUrl: 'http://downloadicons.net/sites/default/files/identification-search-magnifying-glass-icon-73159.png',
    geoTypes: []
    // '(regions)', '(cities)'
  };
  public loader = false;

  constructor(
    private ngxService: NgxUiLoaderService,
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private auth: AuthenticationService,
    private toasts: ToastNotifications
  ) {
    this.route.params.subscribe(params => {
      if (params.id) {
        this.dealer_id = params.id;
        this.getDealerDetail(params.id);
      }
    });
  }

  getDealerDetail(id) {
    this.auth.dealerDetail(id).subscribe(
      res => {
        this.traderLicenceImg = res.dealer_business.licence;
        this.licenceImg = res.dealer_business.owner.licence;
        this.registrationForm.get('about_us').setValue(res.dealer_business.about_us);
        this.registrationForm.get('business_time').setValue(res.dealer_business.open_close_time);
        this.registrationForm.get('business_name').setValue(res.dealer_business.name);
        this.registrationForm.get('business_gstin').setValue(res.dealer_business.gstin);
        this.registrationForm.get('business_nzbn').setValue(res.dealer_business.nzbn);
        this.registrationForm.get('business_moter_vahicle_trader_number').setValue(res.dealer_business.moter_vahicle_trader_number);
        this.registrationForm.get('business_address').setValue(res.dealer_business.address);
        this.registrationForm.get('business_primary_email').setValue(res.dealer_business.primary_email);
        this.registrationForm.get('business_primary_mobile').setValue(res.dealer_business.primary_mobile);
        this.registrationForm.get('business_secondary_mobile').setValue(res.dealer_business.secondary_mobile);
        this.registrationForm.get('business_website').setValue(res.dealer_business.website);
        // tslint:disable-next-line:no-angle-bracket-type-assertion
        (<HTMLInputElement>document.getElementById('search_places')).value = res.dealer_business.address;
      }
    );
  }

  autoCompleteCallback1(eventGoogle) {
    let city = 'New Zealand';
    let region = 'New Zealand';
    eventGoogle.data.address_components.forEach(element => {
      if (element.types[0] === 'locality') {
        city = element.long_name;
      }
      if (element.types[0] === 'administrative_area_level_1') {
        region = element.long_name;
      }
      if (city === 'New Zealand') {
        city = region;
      }
    });

    this.registrationForm.get('business_address').setValue(eventGoogle.data.description);
    this.registrationForm.get('business_city').setValue(city);
    this.registrationForm.get('business_region').setValue(region);
  }

  get f() { return this.registrationForm.controls; }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      business_time: ['', [Validators.required]],
      about_us: ['', [Validators.required]],
      // licence: [''],
      // is_dealer: [''],
      business_name: ['', [Validators.required]],
      // business_gstin: ['', [Validators.required]],
      // business_nzbn: ['', [Validators.required, Validators.maxLength(13)]],
      // business_moter_vahicle_trader_number: ['', [Validators.required]],
      business_address: [''],
      business_city: [''],
      business_region: [''],
      business_primary_email: ['', [Validators.required, Validators.email]],
      business_primary_mobile: ['', [Validators.required]],
      // business_secondary_email: ['', [Validators.email]],
      business_secondary_mobile: [''],
      // business_licence: [''],
      company_logo: [''],
      business_website: [''],
    });
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

  onSubmit() {
    this.submitted = true;
    if (this.registrationForm.invalid) {
      console.log('invalid');
      return; // stop here if form is invalid
    }
    let data = {
      owner_id: this.localStorage.getItem('used_id'),
      open_close_time: this.registrationForm.value.business_time,
      about_us: this.registrationForm.value.about_us,
      city: this.registrationForm.value.business_city,
      region: this.registrationForm.value.business_region,
      name: this.registrationForm.value.business_name,
      gstin: this.registrationForm.value.business_gstin,
      nzbn: this.registrationForm.value.business_nzbn,
      moter_vahicle_trader_number: this.registrationForm.value.business_moter_vahicle_trader_number,
      address: this.registrationForm.value.business_address,
      website: this.registrationForm.value.business_website,
      primary_email: this.registrationForm.value.business_primary_email,
      primary_mobile: this.registrationForm.value.business_primary_mobile,
      secondary_mobile: this.registrationForm.value.business_secondary_mobile,
      licence: this.traderLicenceImg,
      user_licence: this.licenceImg,
      company_logo: this.company_logo_img,
    };
    this.loader = true;
    if (this.dealer_id) {
      this.auth.dealerUpdate(this.dealer_id, data).subscribe(
        res => {
          this.loader = false;
          this.toasts.next({ text: 'Successfully updated', caption: 'Success', type: 'success' });
          setTimeout(() => {
            this.router.navigate(['/myaccount']);
          }, 3000);
        }, error => {
          this.loader = false;
          if (error.status === 422) {
            this.toasts.next({ text: 'Email already registered', caption: 'Fail', type: 'danger' });
          } else {
            this.toasts.next({ text: 'Unable to update, Please try again', caption: 'Fail', type: 'danger' });
          }
        }
      );
    } else {
      this.auth.dealerRegister(data).subscribe(
        res => {
          this.loader = false;
          this.toasts.next({ text: 'Successfully registered', caption: 'Success', type: 'success' });
          this.sendMailToKwikzAdmin(res.dealer_business);
          this.sendMailToDealer(res.dealer_business);
          setTimeout(() => {
            this.router.navigate(['/thank-you']);
          }, 3000);
        }, error => {
          this.loader = false;
          if (error.status === 422) {
            this.toasts.next({ text: 'Email already registered', caption: 'Fail', type: 'danger' });
          } else {
            this.toasts.next({ text: 'Unable to register, Please try again', caption: 'Fail', type: 'danger' });
          }
        }
      );
    }
  }

  dealerLicence(evt) {
    this.licence = evt.target.files;
    this.saveDealerLicenceImg(this.licence);
  }

  tradeLicence(evt) {
    this.traderLicence = evt.target.files;
    this.saveTreaderLicenceImg(this.traderLicence);
  }

  companyLogo(evt) {
    this.company_logo = evt.target.files;
    this.saveCompanyLogoImg(this.company_logo);
  }

  saveCompanyLogoImg(files) {
    this.ngxService.startLoader('company-logo-loader');
    for (let file of files) {
      this.loader = true;
      let params = {
        Bucket: 'bhaskar-kwikz',
        Key: 'dealer' + '/' + Date.now() + '-' + Math.floor(Math.random() * (+9999999 - +100)) + +100 + '-' + file.name,
        Body: file,
        ACL: 'public-read',
        StorageClass: 'REDUCED_REDUNDANCY'
      };
      this.bucket.upload(params, (err, data) => {
        this.ngxService.stopLoader('company-logo-loader');
        if (err) {
          console.log('There was an error uploading your file: ', err);
          this.loader = false;
          return false;
        }
        this.company_logo_img = data.Location;
        this.loader = false;
      });
    }
  }

  saveDealerLicenceImg(files) {
    this.ngxService.startLoader('licence-loader');
    for (let file of files) {
      this.loader = true;
      let params = {
        Bucket: 'bhaskar-kwikz',
        Key: 'dealer' + '/' + Date.now() + '-' + Math.floor(Math.random() * (+9999999 - +100)) + +100 + '-' + file.name,
        Body: file,
        ACL: 'public-read',
        StorageClass: 'REDUCED_REDUNDANCY'
      };
      this.bucket.upload(params, (err, data) => {
        this.ngxService.stopLoader('licence-loader');
        if (err) {
          console.log('There was an error uploading your file: ', err);
          this.loader = false;
          return false;
        }
        this.licenceImg = data.Location;
        this.loader = false;
      });
    }
  }

  saveTreaderLicenceImg(files) {
    this.ngxService.startLoader('treader-loader');
    for (let file of files) {
      this.loader = true;
      let params = {
        Bucket: 'bhaskar-kwikz',
        Key: 'dealer' + '/' + Date.now() + '-' + Math.floor(Math.random() * (+9999999 - +100)) + +100 + '-' + file.name,
        Body: file,
        ACL: 'public-read',
        StorageClass: 'REDUCED_REDUNDANCY'
      };
      this.bucket.upload(params, (err, data) => {
        this.ngxService.stopLoader('treader-loader');
        if (err) {
          console.log('There was an error uploading your file: ', err);
          this.loader = false;
          return false;
        }
        this.traderLicenceImg = data.Location;
        this.loader = false;
      });
    }
  }

  sendMailToKwikzAdmin(data) {
    data['email'] = data.primary_email;
    this.auth.sendDealerRegistrationAdminMail(data).subscribe(
      res => {

      }
    );
  }

  sendMailToDealer(data) {
    data['email'] = data.primary_email;
    this.auth.sendDealerRegistrationMail(data).subscribe(
      res => {
      }
    );
  }

}
