import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dealer-thank-you',
  templateUrl: './dealer-thank-you.component.html',
  styleUrls: ['./dealer-thank-you.component.scss']
})
export class DealerThankYouComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

}
