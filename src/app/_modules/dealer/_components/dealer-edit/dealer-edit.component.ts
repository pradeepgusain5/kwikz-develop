import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpServiceService } from '../../../../_services';

@Component({
  selector: 'app-dealer-edit',
  templateUrl: './dealer-edit.component.html',
  styleUrls: ['./dealer-edit.component.scss']
})
export class DealerEditComponent implements OnInit {

  dealerId: any;
  dealerData: any;
  userData: any;
  public loader = false;

  constructor(private http: HttpServiceService, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.dealerId = params.id;
    });
  }

  ngOnInit() {
    this.loader = true;
    this.http.get('user/detail').subscribe(
      data => {
        this.dealerData = data.dealer_business;
        this.userData = data.user_detail;
        this.loader = false;
      }
    );
    this.scrollUpToTop();
  }

  scrollUpToTop() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
  }

}
