import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostUrl } from '../_pipes/post-url.pipe';
import { LoaderComponent } from '../_helpers/loader/loader.component';

@NgModule({
  declarations: [
    PostUrl,
    LoaderComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    PostUrl,
    LoaderComponent
  ]
})
export class SharedModule { }
